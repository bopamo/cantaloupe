//  Created by Bopamo Osaisai
//  Copyright © 2016 Bopamo Osaisai. All rights reserved.

#include "balance_test.h"
#include "mpu9250_register_map.h"
#include "mpu9250.h"


#include "MadgwickAHRS.h"



#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>



#include "gpio.h"
#include "twi.h"
#include "timer.h"
#include "ptpi_bsp.h"
#include "nrf_drv_twi.h"
#include "nrf_gpio.h"
#include "nrf_drv_gpiote.h"

#include "nrf_drv_rtc.h"

#include "nrf_delay.h"

#include "app_error.h"
#include "sdk_errors.h"


#include "nrf_log.h"
#include "nrf_log_ctrl.h"

// for semi-hosting
// extern void initialise_monitor_handles(void);
#include <stdio.h>

// globals for current state

#define F_ZERO 20.0f

#define PI 3.14159265358979323846f


volatile bool zero_crossed = false;

volatile bool timing_cycle = false;

static uint32_t ms_count = 0;



volatile bool balance_test_data_ready = false;


uint32_t time_start = 0;
uint32_t time_end = 0;
uint32_t time_generated_ticks = 0;


static volatile uint32_t isr_ticks = 0;

uint32_t time_elapsed = 0;

uint32_t t_begin = 0;
uint32_t t_interval = 0;


uint32_t ms_tick = 0;

uint32_t rpm = 0;



static accel_t accel_g;

static temp_t mpu_temp_c;

static gyro_t gyro_rad;

static mag_t magn_gs;

static gyro_t zero_cross[2] =  {F_ZERO};

static float zero_cross_yaw[2] =  {F_ZERO};


static float pitch, yaw, roll;



#define RTT_OUTPUT_BUFFER_SIZE 200

static char rtt_buffer [RTT_OUTPUT_BUFFER_SIZE];




const nrf_drv_rtc_t rtc = NRF_DRV_RTC_INSTANCE(0); /**< Declaring an instance of nrf_drv_rtc for RTC0. */




static void calculate_time_elapsed(void);

static void get_rpm(void);


// i2c device addresses
// #define MPU6050_ADDRESS  0b1101000
// #define HMC5883L_ADDRESS 0b0011110
// #define MS5611_ADDRESS   0b1110111

uint32_t mpu6050_read_sensors(void);


static void toEulerianAngle(void);




/** @brief: Function for handling the RTC0 interrupts.
 * Triggered on TICK and COMPARE0 match.
 */
static void rtc_handler(nrf_drv_rtc_int_type_t int_type)
{

}


/** @brief Function initialization and configuration of RTC driver instance.
 */
static uint32_t rtc_config(void)
{
        uint32_t err_code = NRF_SUCCESS;

        //Initialize RTC instance
        nrf_drv_rtc_config_t config = NRF_DRV_RTC_DEFAULT_CONFIG;
        config.prescaler = 0;
        err_code = nrf_drv_rtc_init(&rtc, &config, rtc_handler);
        APP_ERROR_CHECK(err_code);

        //Enable tick event & interrupt
        nrf_drv_rtc_tick_enable(&rtc, 0);

        //Set compare channel to trigger interrupt after COMPARE_COUNTERTIME seconds
        // err_code = nrf_drv_rtc_cc_set(&rtc,0,COMPARE_COUNTERTIME * 8,true);
        // APP_ERROR_CHECK(err_code);

        //Power on RTC instance
        nrf_drv_rtc_enable(&rtc);

        return err_code;
}


void ride_int_handler(void)
{

        isr_ticks = nrf_drv_rtc_counter_get(&rtc);
        // time_elapsed = ticks_to_ms(time_generated_ticks);

        nrf_drv_rtc_counter_clear(&rtc);

        balance_test_data_ready = true;

}



/**
 * Draws a horizontal ASCII line graph to represent a numerical value, like this:
 *
 * X Acceleration     [                      *                                     ]    -0.985 G
 *
 * @param name    A c-string to show at the left of the graph
 * @param value   The value to be graphed and also shown at the right of the graph
 * @param unit    The unit to be shown at the right of the graph
 * @param min     Sets the scale of the graph
 * @param max     Sets the scale of the graph
 */
void print_graph(char name[], double value, char unit[], double min, double max) {

  #define GRAPH_LENGTH 20

        double percentage = (value - min) / (max - min);
        int dot_location = (GRAPH_LENGTH - 1.0) * percentage;
        printf("%s [", name);
        for (int i = 0; i < GRAPH_LENGTH; i++)
                if (i == dot_location)
                        printf("*");
                else
                        printf(" ");
        printf("] %+9.3f %s\n", value, unit);

}

void balance_test(void) {

        uint32_t err_code;

        int count = 0;

        err_code = mpu9250_setup();
        APP_ERROR_CHECK(err_code);

        nrf_delay_ms(100);

        err_code = gpio_interrupts_init();
        APP_ERROR_CHECK(err_code);

        // configure the MPU6050 (gyro/accelerometer)
//  i2c_write_register(I2C1, MPU6050_ADDRESS,  0x6B, 0x00);                    // exit sleep
//  i2c_write_register(I2C1, MPU6050_ADDRESS,  0x19, 109);                     // sample rate = 8kHz / 110 = 72.7Hz
//  i2c_write_register(I2C1, MPU6050_ADDRESS,  0x1B, 0x18);                    // gyro full scale = +/- 2000dps
//  i2c_write_register(I2C1, MPU6050_ADDRESS,  0x1C, 0x08);                    // accelerometer full scale = +/- 4g
//  i2c_write_register(I2C1, MPU6050_ADDRESS,  0x38, 0x01);                    // enable INTA interrupt
//
//
//  // configure the MPU6050 to automatically read the magnetometer
//  i2c_write_register(I2C1, MPU6050_ADDRESS,  0x25, HMC5883L_ADDRESS | 0x80); // slave 0 i2c address, read mode
//  i2c_write_register(I2C1, MPU6050_ADDRESS,  0x26, 0x03);                    // slave 0 register = 0x03 (x axis)
//  i2c_write_register(I2C1, MPU6050_ADDRESS,  0x27, 6 | 0x80);                // slave 0 transfer size = 6, enabled
//  i2c_write_register(I2C1, MPU6050_ADDRESS,  0x67, 1);                       // enable slave 0 delay
//
// //	// configure the MS5611 (barometer)
// //	i2c_write_register(I2C1, MS5611_ADDRESS,   0x1E, 0x00);                    // reset
// //	i2c_write_register(I2C1, MS5611_ADDRESS,   0x48, 0x00);                    // start conversion of the pressure sensor
//
//  // configure an external interrupt for the MPU6050's active-high INTA signal
//  gpio_setup(PB7, INPUT, PUSH_PULL, FIFTY_MHZ, NO_PULL, AF0);
//  exti_setup(PB7, RISING_EDGE);

//	// configure an external interrupt for the MS5611's active-low DRDY signal
//	gpio_setup(PB6, INPUT, PUSH_PULL, FIFTY_MHZ, NO_PULL, AF0);
//	exti_setup(PB6, FALLING_EDGE);


// Clear the screen before we start GUI
        printf("\033[H\033[J");

        // enter an infinite loop that prints the sensor readings out to the PC
        while(1)
        {
                if(balance_test_data_ready)
                {
                        // Make sure any pending events are cleared
                        __SEV();
                        __WFE();
                        // Enter System ON sleep mode
                        __WFE();
                        NRF_LOG_FLUSH();
                }



                err_code = mpu6050_read_sensors();
                APP_ERROR_CHECK(err_code);



                printf("%d,%10f,%10f,%10f\n", count++, gyro_rad.x, gyro_rad.y, gyro_rad.z);








//                 print_graph("X Acceleration   ", accel_x_g, "G", -4.0, 4.0);
//                 print_graph("Y Acceleration   ", accel_y_g, "G", -4.0, 4.0);
//                 print_graph("Z Acceleration   ", accel_z_g, "G", -4.0, 4.0);
//                 printf("\n");
//                 // print_graph("MPU6050 Temp     ", mpu_temp_c, "C", -150.0, 150.0);
//                 printf("\n");
//                 // print_graph("X Rotation       ", gyro_x_rad, "Rad/s", -34.91, 34.91);
//                 // print_graph("Y Rotation       ", gyro_y_rad, "Rad/s", -34.91, 34.91);
//                 // print_graph("Z Rotation       ", gyro_z_rad, "Rad/s", -34.91, 34.91);
                // print_graph("X Rotation       ", gyro_rad.x, "d/s", -250, 250);
                // print_graph("Y Rotation       ", gyro_rad.y, "d/s", -250, 250);
                // print_graph("Z Rotation       ", gyro_rad.z, "d/s", -250, 250);
                // printf("\n");
//                 // print_graph("X Magnetic Field ", magn_x_gs, "Gs", -2.5, 2.5);
//                 // print_graph("Y Magnetic Field ", magn_y_gs, "Gs", -2.5, 2.5);
//                 // print_graph("Z Magnetic Field ", magn_z_gs, "Gs", -2.5, 2.5);
//                 printf("\n");
// //		print_graph("Pressure         ", pressure_float, " ", 0.0, 10000000.0);
// //		print_graph("MS5611 Temp      ", baro_temp_float, " ", 0.0, 10000000.0);
                // printf("\x1B[4A"); // VT100 escape code: move cursor up 14 lines
//                 // printf("\033[2J\033[;H");
// //                fflush(stdout);


                balance_test_data_ready = false;

        }

}

uint32_t mpu6050_read_sensors(void) {

        uint32_t err_code;


        // read the sensor values
        static temp_value_t t;

        static accel_values_t a;

        static gyro_values_t g;

        static mag_values_t m;


        err_code = mpu9250_get_data((int16_t*)(&a), (int16_t*)(&g), (int16_t*)(&t));
        APP_ERROR_CHECK(err_code);

        //  err_code = mpu9250_read_mag((int16_t*)(&m));
        //  APP_ERROR_CHECK(err_code);


        // extract the raw values

        // convert accelerometer readings into G's
        // accel_x_g = accel_x / 8192.0f;
        // accel_y_g = accel_y / 8192.0f;
        // accel_z_g = accel_z / 8192.0f;


        mpu9250_convert_raw_accel(&accel_g, &a);

        // convert temperature reading into degrees Celsius
        // mpu_temp_c = t / 340.0f + 36.53f;

        // convert gyro readings into Radians per second
        // gyro_x_rad = gyro_x / 939.650784f;
        // gyro_y_rad = gyro_y / 939.650784f;
        // gyro_z_rad = gyro_z / 939.650784f;


        // err_code = mpu9250_read_gyro((int16_t*)(&g));
        // APP_ERROR_CHECK(err_code);



        mpu9250_convert_raw_gyro(&gyro_rad, &g);

        ms_count += ticks_to_ms(isr_ticks);

        // MadgwickAHRSupdateIMU(gyro_rad.x, gyro_rad.y, gyro_rad.z, accel_g.x, accel_g.y, accel_g.z);
        //
        // toEulerianAngle();

        if( zero_cross[0].z == F_ZERO ) {

                zero_cross[0] = gyro_rad;

        }

        else {

                zero_cross[1] = zero_cross[0];
                zero_cross[0] = gyro_rad;

        }



        if( ms_count > 500 ) {

                if (signbit(zero_cross[0].z) && !signbit(zero_cross[1].z) ) {

                        calculate_time_elapsed();
                        zero_crossed = true;
                }

                else {

                        // time_elapsed = 0;
                        zero_crossed = false;

                }


                // if( zero_cross_yaw[0] == F_ZERO ) {
                //
                //         zero_cross_yaw[0] = yaw;
                //
                // }
                //
                // else {
                //
                //         zero_cross_yaw[1] = zero_cross_yaw[0];
                //         zero_cross_yaw[0] = yaw;
                //
                // }
                //
                //
                //
                // if( ms_count > 500 ) {
                //
                //         if (signbit(zero_cross_yaw[0]) && !signbit(zero_cross_yaw[1]) ) {
                //
                //                 calculate_time_elapsed();
                //                 zero_crossed = true;
                //         }
                //
                //         else {
                //
                //                 // time_elapsed = 0;
                //                 zero_crossed = false;
                //
                //         }
                //









        }








        // convert magnetometer readings into Gauss's
        // magn_x_gs = magn_x / 660.0f;
        // magn_y_gs = magn_y / 660.0f;
        // magn_z_gs = magn_z / 660.0f;

        // mpu9250_convert_raw_magn(&magn_gs, &m);

        return err_code;

}



void ride(void)
{


        uint32_t err_code;

        int16_t tempCount; // Stores the real internal chip temperature in degrees Celsius
        float temperature;

        uint8_t new_data = 0;

        static int16_t raw_gyro[3];
        static uint16_t raw_adc[8];


        static char rtt_input = 0;

        int rtt_buffer_count;

        err_code = mpu9250_setup();
        APP_ERROR_CHECK(err_code);

        nrf_delay_ms(100);



        err_code = gpio_interrupts_init();
        APP_ERROR_CHECK(err_code);



        err_code = rtc_config();
        APP_ERROR_CHECK(err_code);

        // initialize ms_count which gets incremented in mpu9250_int_pin_handler
        ms_count = 0;



        // t_begin = app_timer_cnt_get();
        // Clear the screen before we start GUI
        printf("\033[H\033[J");

        printf("  n,  t,  rpm,  x,  y,  z, yaw, pitch, roll, zero, margins\r\n");
        // enter an infinite loop that prints the sensor readings out to the PC
        while(1)
        {
                if(!balance_test_data_ready)
                {
                        // Make sure any pending events are cleared
                        __SEV();
                        __WFE();
                        // Enter System ON sleep mode
                        __WFE();
                        NRF_LOG_FLUSH();
                }



                rtt_input = SEGGER_RTT_HasKey();

                if (rtt_input) {

                        if (SEGGER_RTT_GetKey() == 'r') {

                                SEGGER_RTT_printf(0, "%sResetting in %d second..%s\n", RTT_CTRL_BG_BRIGHT_RED, 1, RTT_CTRL_RESET);
                                nrf_delay_ms(1000);
                                sd_nvic_SystemReset();

                        }
                }



                // err_code = mpu9250_read_gyro(raw_gyro);
                // APP_ERROR_CHECK(err_code);

                err_code = mpu9250_clear_interrupts();
                APP_ERROR_CHECK(err_code);



                err_code = mpu6050_read_sensors();
                APP_ERROR_CHECK(err_code);


                // MadgwickAHRSupdateIMU(gyro_rad.x, gyro_rad.y, gyro_rad.z, accel_g.x, accel_g.y, accel_g.z);


                // Define output variables from updated quaternion---these are Tait-Bryan angles, commonly used in aircraft orientation.
                // In this coordinate system, the positive z-axis is down toward Earth.
                // Yaw is the angle between Sensor x-axis and Earth magnetic North (or true North if corrected for local declination, looking down on the sensor positive yaw is counterclockwise.
                // Pitch is angle between sensor x-axis and Earth ground plane, toward the Earth is positive, up toward the sky is negative.
                // Roll is angle between sensor y-axis and Earth ground plane, y-axis up is positive roll.
                // These arise from the definition of the homogeneous rotation matrix constructed from quaternions.
                // Tait-Bryan angles as well as Euler angles are non-commutative; that is, the get the correct orientation the rotations must be
                // applied in the correct order which for this configuration is yaw, pitch, and then roll.
                // For more see http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles which has additional links.
                // yaw   = atan2(2.0f * (q1 * q2 + q0 * q3), q0 * q0 + q1 * q1 - q2 * q2 - q3 * q3);
                // pitch = -asin(2.0f * (q1 * q3 - q0 * q2));
                // roll  = atan2(2.0f * (q0 * q1 + q2 * q3), q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3);
                // pitch *= 180.0f / PI;
                // yaw   *= 180.0f / PI;
                // // yaw   -= 13.8f;// Declination at Danville, California is 13 degrees 48 minutes and 47 seconds on 2014-04-04
                // roll  *= 180.0f / PI;

                // toEulerianAngle();




                //  printf("%d,%10f,%10f,%10f\n", ms_count++, gyro_rad.x, gyro_rad.y, gyro_rad.z);

                // raw_gyro[0] = gyro_rad.x;
                // raw_gyro[1] = gyro_rad.y;
                // raw_gyro[2] = gyro_rad.z;





                // new_data = process_raw_data((float*)&gyro_rad, raw_adc, ms_count++);

                // get_rpm();

                // t_interval = app_timer_cnt_get();
                // err_code = app_timer_cnt_diff_compute(t_interval, t_begin, &time_generated_ticks);
                // ms_tick = ticks_to_ms(time_generated_ticks);


                // printf("%d,%d,%d,", ms_count, time_elapsed, rpm );
                // printf("%f,%f,%f,", gyro_rad.x, gyro_rad.y, gyro_rad.z);
                // printf("%f,%f,%f,", yaw, pitch, roll);

                rtt_buffer_count = snprintf(rtt_buffer, RTT_OUTPUT_BUFFER_SIZE, "%d,%d,%d,%f,%f,%f,%f,%f,%f,", ms_count, time_elapsed, rpm, gyro_rad.z, q0 * (180.0f / PI), q1 * (180.0f / PI), q2 * (180.0f / PI), q3 * (180.0f / PI), yaw);

                printf("%s", rtt_buffer);


                if( zero_crossed || timing_cycle ) {

                        if( zero_crossed ) printf("2,200\r\n");

                        else printf("2,0\r\n");


                }

                else printf("0,0\r\n");



                balance_test_data_ready = false;
                // NRF_WDT->RR[0] = WDT_RR_RR_Reload;

        }


}




static void calculate_time_elapsed(void)
{

        uint32_t err_code;


        uint32_t t_interval;


        if( !timing_cycle ) {

                time_start = ms_count;

                // nrf_drv_rtc_counter_clear(&rtc);
                // time_elapsed = 0;
                timing_cycle = true;

        }


        else {
                time_end = ms_count;
                // err_code = app_timer_cnt_diff_compute(time_end, time_start, &time_generated_ticks);

                // time_generated_ticks = nrf_drv_rtc_counter_get(&rtc);
                // time_elapsed = ticks_to_ms(time_generated_ticks);

                time_elapsed = time_end - time_start;

                get_rpm();

                timing_cycle = false;

                // time_start = 0;

        }

}


static void get_rpm(void)
{

        if( !time_elapsed )
                rpm = 0;

        else
                rpm = ( 60 * 1000 ) / ( time_elapsed );


}



static void toEulerianAngle(void)
{

        // q.w() = q0
        // q.x() = q1
        // q.y() = q2
        // q.z() = q3

        double ysqr = q2 * q2;
        double t0 = -2.0f * (ysqr + q3 * q3) + 1.0f;
        double t1 = +2.0f * (q1 * q2 - q0 * q3);
        double t2 = -2.0f * (q1 * q3 + q0 * q2);
        double t3 = +2.0f * (q2 * q3 - q0 * q1);
        double t4 = -2.0f * (q1 * q1 + ysqr) + 1.0f;

        t2 = t2 > 1.0f ? 1.0f : t2;
        t2 = t2 < -1.0f ? -1.0f : t2;

        pitch = asin(t2);
        roll  = atan2(t3, t4);
        yaw   = atan2(t1, t0);

        pitch *= 180.0f / PI;
        yaw   *= 180.0f / PI;
        // yaw   -= 13.8f;// Declination at Danville, California is 13 degrees 48 minutes and 47 seconds on 2014-04-04
        roll  *= 180.0f / PI;
}
