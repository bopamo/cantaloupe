

#ifndef PTPI_BSP_H__
#define PTPI_BSP_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include "app_util_platform.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "SEGGER_RTT.h"


/**
 * Definitions needed for PTPI build
 */


// #define USING_NRF51_DK

#ifndef FIRMWARE_VERSION
  #define FIRMWARE_VERSION    "1.2.0"
#endif

#ifndef PTPI_RX_PIN_NUMBER
  #define PTPI_RX_PIN_NUMBER  29
#endif

#ifndef PTPI_TX_PIN_NUMBER
  #define PTPI_TX_PIN_NUMBER  28
#endif

#ifndef PTPI_POWER_LED
  #ifdef USING_NRF51_DK
    #define PTPI_POWER_LED    21
  #else
    #define PTPI_POWER_LED    16
   #endif
#endif


#ifndef PTPI_TWI_SCL_PIN
  #define PTPI_TWI_SCL_PIN      8
#endif


#ifndef PTPI_TWI_SDA_PIN
  #define PTPI_TWI_SDA_PIN      9
#endif


#ifndef PTPI_MPU_INT_PIN
  #define PTPI_MPU_INT_PIN      15
#endif

#ifndef PTPI_COUNTER_INT_PIN
  #define PTPI_COUNTER_INT_PIN  12
#endif

//#ifndef NRF_LOG_COLOR_WHITE
//#define NRF_LOG_COLOR_WHITE     RTT_CTRL_TEXT_WHITE
//#endif
//
//#ifndef NRF_LOG_COLOR_RED
//#define NRF_LOG_COLOR_RED       RTT_CTRL_TEXT_RED
//#endif
//
//#ifndef NRF_LOG_COLOR_DEFAULT
//#define NRF_LOG_COLOR_DEFAULT   RTT_CTRL_TEXT_BLACK
//#endif
//
#ifndef NRF_LOG_MODULE_NAME
  #define NRF_LOG_MODULE_NAME "PTPI"
#endif




#ifndef DEBUG_FUNCTION_ENTER
  #define DEBUG_FUNCTION_ENTER NRF_LOG_RAW_INFO("\r\n\r\n");                      \
        NRF_LOG_DEBUG("%sENTER: %s()%s", (uint32_t)("\t\t\t\t\t\t\t\t\t"),     \
                                            (uint32_t)(__func__),                 \
                                            (uint32_t)("\r\n\r\n"));          \
                                            NRF_LOG_FLUSH();
#endif


#ifndef DEBUG_FUNCTION_EXIT
  #define DEBUG_FUNCTION_EXIT NRF_LOG_RAW_INFO("\r\n\r\n");                       \
        NRF_LOG_DEBUG("%sLEAVING... %s()%s", (uint32_t)("\t\t\t\t\t\t\t\t\t"), \
                                                (uint32_t)(__func__),             \
                                                (uint32_t)("\r\n\r\n"));        \
                                                NRF_LOG_FLUSH();
#endif


#ifndef DEBUG_INFINITE_LOOP
  #define DEBUG_INFINITE_LOOP NRF_LOG_RAW_INFO("\r\n\r\n");                           \
        NRF_LOG_DEBUG("%s%s%s()%s", (uint32_t)("\t\t\t\t\t\t\t\t\t*******\t\t"),  \
                                        (uint32_t)("ENTERING INFINITE LOOP in "),     \
                                        (uint32_t)(__func__),                         \
                                        (uint32_t)("\t\t*******\r\n\r\n"));         \
                                        NRF_LOG_FLUSH();
#endif



#ifdef __cplusplus
}
#endif

#endif //PTPI_BSP_H__

/** @} */
