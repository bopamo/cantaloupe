# README #

This repo contains some sample code from a project I worked on at the end of 2016 and up until last fall (2017).
The code is from a data logger used to read MPU (https://www.invensense.com/products/motion-tracking/9-axis/mpu-9250/) data
from a wearable so we could condition the filters depending on the mode of operation.
I had to port an Arduino version of the MPU driver over to the nRF51.
The goal of the implementation was to get the port working as quickly as possible with the new hardware so I cut corners in terms of testing and documentation because
the design was intended for an intermediate hardware revision and many of the hardware components had not been finalized.

I typically have a private repo setup for each of my projects and use code coverage tools (COVTOOL or gocv), static analysis tools (clang or cppcheck), along with CI integration on git commits.

In the past I would just give access to full repos of projects I’m currently working on,
but I’ve gotten trouble for that and now I just share relevant files from projects I’ve previously worked on that aren’t under any NDA restrictions.

I have attached the most relevant files and I’m happy to discuss/answer/clarify anything related to them.
