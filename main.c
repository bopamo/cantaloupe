/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */


#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>



#include "app_uart.h"
#include "app_error.h"
#include "nrf_delay.h"
#include "nrf.h"
#include "bsp.h"
#include "app_error.h"

#include "ptpi_bsp.h"

#include "nrf_drv_twi.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "SEGGER_RTT.h"



#include "uart.h"
#include "gpio.h"
#include "timer.h"
#include "led.h"
#include "twi.h"
#include "mpu9250.h"
#include "sensor_processor.h"

#include "balance_test.h"

#include "csc.h"





void print_firmware_version(void);


/**
 * @brief Function for main application entry.
 */
int main(void)
{

        uint32_t err_code;

        uint32_t time_ticks;

      // Initialize various modules

        err_code = NRF_LOG_INIT(NULL);
        APP_ERROR_CHECK(err_code);

        NRF_LOG_INFO("  NRF LOG INITIALIZED...\r\n");
        NRF_LOG_FLUSH();


        NRF_LOG_RAW_INFO("  FW Version: %s\r\n", (uint32_t)(FIRMWARE_VERSION));
        NRF_LOG_FLUSH();

        NRF_LOG_DEBUG("   LED BUTTON : %d\r\n", (uint32_t)(PTPI_POWER_LED));
        NRF_LOG_FLUSH();


        err_code = uart_init();
        APP_ERROR_CHECK(err_code);

        //    uart_tx_test()


        err_code = gpio_init();
        APP_ERROR_CHECK(err_code);

        // err_code = gpio_interrupts_init();
        // APP_ERROR_CHECK(err_code);

        //    gpio_test();

        err_code = timer_init();
        APP_ERROR_CHECK(err_code);


        err_code = led_init();
        APP_ERROR_CHECK(err_code);

        err_code = twi_init();
        APP_ERROR_CHECK(err_code);

        //    twi_test();

        err_code = application_timers_start();
        APP_ERROR_CHECK(err_code);


        // mpu9250_test();

      //  mpu9250_interrupt_test();

        // balance_test();

        ride();



        // Going into low power mode
        while (true)
        {
                // Make sure any pending events are cleared
                __SEV();

                __WFE();

                __WFE();
                NRF_LOG_FLUSH();
        }


//*/


}


/** @} */



void print_firmware_version(void) {

        printf(" \r\nFW Version: %s\r\n", FIRMWARE_VERSION);

}
