
#ifndef BALANCE_TEST_H__
#define BALANCE_TEST_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include "app_util_platform.h"



// perform balance test on PTPI
void balance_test(void);

// Interupt handler required
void ride_int_handler(void);

// Function called for PTPI data acquisition during a "ride"
void ride(void);



#ifdef __cplusplus
}
#endif

#endif // BALANCE_TEST_H__

/** @} */
