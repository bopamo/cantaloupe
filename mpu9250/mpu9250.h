#ifndef MPU9250_H
#define MPU9250_H


#ifdef __cplusplus
extern "C" {
#endif


#include <stdint.h>
#include <stdbool.h>


#include "app_util_platform.h"
#include "nrf_drv_twi.h"
#include "nrf_drv_gpiote.h"



// Set initial input parameters
enum Ascale {
        AFS_2G = 0,
        AFS_4G,
        AFS_8G,
        AFS_16G
};

enum Gscale {
        GFS_250DPS = 0,
        GFS_500DPS,
        GFS_1000DPS,
        GFS_2000DPS
};

enum Mscale {
        MFS_14BITS = 0, // 0.6 mG per LSB
        MFS_16BITS // 0.15 mG per LSB
};


enum Gyro_DLPF {
        GLPF_250HZ = 0,
        GLPF_184HZ,
        GLPF_92HZ,
        GLPF_41HZ,
        GLPF_20HZ,
        GLPF_10HZ,
        GLPF_5HZ,
        GLPF_3600HZ
};


typedef struct
{
        int16_t z;
        int16_t y;
        int16_t x;
}mag_values_t;


typedef struct
{
        float z;
        float y;
        float x;
}mag_t;




/**@brief Structure to hold acceleromter values.
 * Sequence of z, y, and x is important to correspond with
 * the sequence of which z, y, and x data are read from the sensor.
 * All values are unsigned 16 bit integers
 */
typedef struct
{
        int16_t z;
        int16_t y;
        int16_t x;
}accel_values_t;


typedef struct
{
        float z;
        float y;
        float x;
}accel_t;



/**@brief Structure to hold gyroscope values.
 * Sequence of z, y, and x is important to correspond with
 * the sequence of which z, y, and x data are read from the sensor.
 * All values are unsigned 16 bit integers
 */
typedef struct
{
        int16_t z;
        int16_t y;
        int16_t x;
}gyro_values_t;



typedef struct
{
        float z;
        float y;
        float x;
}gyro_t;


/**@brief Simple typedef to hold temperature values */
typedef int16_t temp_value_t;

typedef float temp_t;

/**@brief MPU driver digital low pass fileter and external Frame Synchronization (FSYNC) pin sampling configuration structure */
typedef struct
{
        uint8_t dlpf_cfg          : 3;// 3-bit unsigned value. Configures the Digital Low Pass Filter setting.
        uint8_t ext_sync_set      : 3;// 3-bit unsigned value. Configures the external Frame Synchronization (FSYNC) pin sampling.
        uint8_t fifo_mode         : 1;// When set to ‘1’, when the fifo is full, additional writes will not be written to fifo. When set to ‘0’, when the fifo is full, additional writes will be written to the fifo, replacing the oldest data.
        uint8_t                   : 1;
}sync_dlpf_config_t;

/**@brief MPU driver gyro configuration structure. */
typedef struct
{
        uint8_t f_choice          : 2;
        uint8_t                   : 1;
        uint8_t fs_sel            : 2;// FS_SEL 2-bit unsigned value. Selects the full scale range of gyroscopes.
        uint8_t gz_st             : 1;
        uint8_t gy_st             : 1;
        uint8_t gx_st             : 1;
}gyro_config_t;


/**@brief MPU driver accelerometer configuration structure. */
typedef struct
{
        uint8_t                   : 3;
        uint8_t afs_sel           : 2;// 2-bit unsigned value. Selects the full scale range of accelerometers.
        uint8_t za_st             : 1;// When set to 1, the Z- Axis accelerometer performs self test.
        uint8_t ya_st             : 1;// When set to 1, the Y- Axis accelerometer performs self test.
        uint8_t xa_st             : 1;// When set to 1, the X- Axis accelerometer performs self test.
}accel_config_t;

/**@brief MPU9255 driver accelerometer second configuration structure. */
typedef struct
{
        uint8_t a_dlpf_cfg        : 2;// 3-bit unsigned value. Selects the Digital High Pass Filter configuration.
        uint8_t accel_f_choice_b  : 2;// 2-bit unsigned value. Selects the full scale range of accelerometers.
        uint8_t                   : 4;
}accel_config_2_t;

#define MPU_DEFAULT_2_CONFIG()                  \
        {                                            \
                .a_dlpf_cfg        = 0,              \
                .accel_f_choice_b  = 0,              \
        }

/**@brief MPU driver general configuration structure. */
typedef struct
{
        uint8_t smplrt_div;                 // Divider from the gyroscope output rate used to generate the Sample Rate for the MPU-9150. Sample Rate = Gyroscope Output Rate / (1 + SMPLRT_DIV)
        sync_dlpf_config_t sync_dlpf_gonfig; // Digital low pass fileter and external Frame Synchronization (FSYNC) configuration structure
        gyro_config_t gyro_config;          // Gyro configuration structure
        accel_config_t accel_config;        // Accelerometer configuration structure
        accel_config_2_t accel_config_2;
}mpu_config_t;

#define MPU_DEFAULT_CONFIG()                          \
        {                                                     \
                .smplrt_div                     = 7,              \
                .sync_dlpf_gonfig.dlpf_cfg      = 1,              \
                .sync_dlpf_gonfig.ext_sync_set  = 0,              \
                .gyro_config.fs_sel             = GFS_500DPS,     \
                .gyro_config.f_choice           = 3,              \
                .gyro_config.gz_st              = 0,              \
                .gyro_config.gy_st              = 0,              \
                .gyro_config.gx_st              = 0,              \
                .accel_config.afs_sel           = AFS_16G,        \
                .accel_config.za_st             = 0,              \
                .accel_config.ya_st             = 0,              \
                .accel_config.xa_st             = 0,              \
        }


/**@brief MPU driver interrupt pin configuration structure. */
typedef struct
{
        uint8_t clkout_en       : 1;// When this bit is equal to 1, a reference clock output is provided at the CLKOUT pin. When this bit is equal to 0, the clock output is disabled. For further information regarding CLKOUT, please refer to the MPU-9150 Product Specification document.
        uint8_t i2c_bypass_en   : 1;// When this bit is equal to 1 and I2C_MST_EN (Register 106 bit[5]) is equal to 0, the host application processor will be able to directly access the auxiliary I2C bus of the MPU-9150. When this bit is equal to 0, the host application processor will not be able to directly access the auxiliary I2C bus of the MPU-9150 regardless of the state of I2C_MST_EN (Register 106 bit[5]).
        uint8_t fsync_int_en    : 1;// When equal to 0, this bit disables the FSYNC pin from causing an interrupt to the host processor. When equal to 1, this bit enables the FSYNC pin to be used as an interrupt to the host processor.
        uint8_t fsync_int_level : 1; // When this bit is equal to 0, the logic level for the FSYNC pin (when used as an interrupt to the host processor) is active high. When this bit is equal to 1, the logic level for the FSYNC pin (when used as an interrupt to the host processor) is active low.
        uint8_t int_rd_clear    : 1;// When this bit is equal to 0, interrupt status bits are cleared only by reading INT_STATUS (Register 58). When this bit is equal to 1, interrupt status bits are cleared on any read operation.
        uint8_t latch_int_en    : 1;// When this bit is equal to 0, the INT pin emits a 50us long pulse. When this bit is equal to 1, the INT pin is held high until the interrupt is cleared.
        uint8_t int_open        : 1;// When this bit is equal to 0, the INT pin is configured as push-pull. When this bit is equal to 1, the INT pin is configured as open drain.
        uint8_t int_level       : 1;// When this bit is equal to 0, the logic level for the INT pin is active high. When this bit is equal to 1, the logic level for the INT pin is active low.
}mpu_int_pin_cfg_t;

/**@brief MPU interrupt pin default configuration. */
#define MPU_DEFAULT_INT_PIN_CONFIG()    \
        {                                   \
                .clkout_en          = 0,    \
                .i2c_bypass_en      = 0,    \
                .fsync_int_en       = 0,    \
                .fsync_int_level    = 0,    \
                .int_rd_clear       = 1,    \
                .latch_int_en       = 0,    \
                .int_open           = 0,    \
                .int_level          = 0,    \
        }


/**@brief MPU driver interrupt source configuration structure. */
typedef struct
{
        uint8_t data_rdy_en     : 1;// When set to 1, this bit enables the Data Ready interrupt, which occurs each time a write operation to all of the sensor registers has been completed.
        uint8_t                 : 2;//
        uint8_t i2c_mst_int_en  : 1;// When set to 1, this bit enables any of the I2C Master interrupt sources to generate an interrupt
        uint8_t fifo_oflow_en   : 1;// When set to 1, this bit enables a FIFO buffer overflow to generate an interrupt.
        uint8_t zmot_en         : 1;// When set to 1, this bit enables Zero Motion detection to generate an interrupt.
        uint8_t mot_en          : 1;// When set to 1, this bit enables Motion detection to generate an interrupt.
        uint8_t ff_en           : 1;// When set to 1, this bit enables Free Fall detection to generate an interrupt.
}mpu_int_enable_t;

/**@brief MPU interrupt sources default configuration. */
#define MPU_DEFAULT_INT_ENABLE_CONFIG() \
        {                               \
                .data_rdy_en    = 0,    \
                .i2c_mst_int_en = 0,    \
                .fifo_oflow_en  = 0,    \
                .zmot_en        = 0,    \
                .mot_en         = 0,    \
                .ff_en          = 0,    \
        }




volatile bool mpu9250_data_ready = false;

volatile bool mpu9250_get_raw_data_from_isr = false;


//===================================================================================================================
//====== Set of useful function to access acceleration, gyroscope, and temperature data
//===================================================================================================================

/**
 * @brief Simple interrupt handler setting a flag indicating that data is ready
 *
 */
void mpu9250_int_pin_handler(void);


/**
 * @brief MPU Timer interrupt handler setting a flag indicating that data is ready
 *
 */
void mpu9250_timer_int_handler(void * p_context);


/**
 * @brief Funtion to perform initialization, calibration AND testing before MPU usage
 *
 */
uint32_t mpu9250_setup(void);


/**@brief Function for initiating MPU9250 and MPU library
 *
 * Resets gyro, accelerometer and temperature sensor signal paths.
 * Function resets the analog and digital signal paths of the gyroscope, accelerometer,
 * and temperature sensors.
 * The reset will revert the signal path analog to digital converters and filters to their power up
 * configurations.
 *
 * @param[in]   p_instance      Instance of nRF5x TWI module
 * @retval      uint32_t        Error code
 */
uint32_t mpu9250_init(void);

// Clear MPU Interrupts
uint32_t mpu9250_clear_interrupts(void);




void mpu9250_convert_raw_accel(accel_t * aOut, accel_values_t * aRaw);

void mpu9250_convert_raw_gyro(gyro_t * gOut,    gyro_values_t * gRaw);

void mpu9250_convert_raw_magn(mag_t * mOut,      mag_values_t  * mRaw);


/**@brief Function for measuring and displaying MPU sensitivity data.
 *
 * @param[in]   void
 * @retval      void       
 */
void mpu9250_sensitivity(void);


/**@brief Function for reading the source of the MPU generated interrupts.
 *
 * @param[in]   int_source      Pointer to variable to hold interrupt source
 * @retval      uint32_t        Error code
 */
// uint32_t mpu9250_read_int_source(uint8_t * int_source);

/**@brief Function for reading MPU WHOAMI.
 *
 * @param[in]   whoami_value     Pointer to variable to hold whoami data
 * @retval      uint32_t        Error code
 */
uint32_t mpu9250_whami(void);


uint32_t mpu9250_read_accel(int16_t * destination);

uint32_t mpu9250_read_gyro(int16_t * destination);


uint32_t mpu9250_read_mag(int16_t * destination);


int16_t mpu9250_read_temp(void);


uint32_t mpu9250_get_data(int16_t * accel, int16_t * gyro, int16_t * temp);




uint32_t mpu9250_get_settings(void);

uint32_t mpu9250_reset(void);

uint32_t mpu9250_init_ak8963(float * destination);

//void calibrateMag(float * dest1, float * dest2, Serial &pc)
uint32_t mpu9250_calibrate_mag(float * dest1, float * dest2);


// Function which accumulates gyro and accelerometer data after device initialization. It calculates the average
// of the at-rest readings and then loads the resulting offsets into accelerometer and gyro bias registers.
uint32_t mpu9250_calibrate_gyro_accel(float * dest1, float * dest2);


// Accelerometer and gyroscope self test; check calibration wrt factory settings
uint32_t mpu9250_self_test(float * destination); // Should return percent deviation from factory trim values, +/- 14 or less deviation is a pass



// Implementation of Sebastian Madgwick's "...efficient orientation filter for... inertial/magnetic sensor arrays"
// (see http://www.x-io.co.uk/category/open-source/ for examples and more details)
// which fuses acceleration, rotation rate, and magnetic moments to produce a quaternion-based estimate of absolute
// device orientation -- which can be converted to yaw, pitch, and roll. Useful for stabilizing quadcopters, etc.
// The performance of the orientation filter is at least as good as conventional Kalman-based filtering algorithms
// but is much less computationally intensive---it can be performed on a 3.3 V Pro Mini operating at 8 MHz!
void MadgwickQuaternionUpdate(float ax, float ay, float az, float gx, float gy, float gz, float mx, float my, float mz);

// Similar to Madgwick scheme but uses proportional and integral filtering on the error between estimated reference vectors and
// measured ones.
void MahonyQuaternionUpdate(float ax, float ay, float az, float gx, float gy, float gz, float mx, float my, float mz);



void mpu9250_test(void);



void mpu9250_interrupt_test(void);

#ifdef __cplusplus
}
#endif

#endif //MPU9250_H
