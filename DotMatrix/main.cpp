

#include <Arduino.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>
#include <SimpleFont5.h>

#undef abs

#include <vector>


#ifndef PSTR
 #define PSTR // Make Arduino Due happy
#endif

#define NEO_PIN     6 // Arduino pin to NeoPixel data input
#define NEO_WIDTH  20 // Hat circumference in pixels
#define NEO_HEIGHT  5 // Number of pixel rows (round up if not equal)


// Color definitions
#define BLACK    0x0000
#define BLUE     0x001F
#define RED      0xF800
#define GREEN    0x07E0
#define CYAN     0x07FF
#define MAGENTA  0xF81F
#define YELLOW   0xFFE0
#define WHITE    0xFFFF


#define BGCOLOR BLACK


// MATRIX DECLARATION:
// Parameter 1 = width of NeoPixel matrix
// Parameter 2 = height of matrix
// Parameter 3 = pin number (most are valid)
// Parameter 4 = matrix layout flags, add together as needed:
//   NEO_MATRIX_TOP, NEO_MATRIX_BOTTOM, NEO_MATRIX_LEFT, NEO_MATRIX_RIGHT:
//     Position of the FIRST LED in the matrix; pick two, e.g.
//     NEO_MATRIX_TOP + NEO_MATRIX_LEFT for the top-left corner.
//   NEO_MATRIX_ROWS, NEO_MATRIX_COLUMNS: LEDs are arranged in horizontal
//     rows or in vertical columns, respectively; pick one or the other.
//   NEO_MATRIX_PROGRESSIVE, NEO_MATRIX_ZIGZAG: all rows/columns proceed
//     in the same order, or alternate lines reverse direction; pick one.
//   See example below for these values in action.
// Parameter 5 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_GRBW    Pixels are wired for GRBW bitstream (RGB+W NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)


// Example for NeoPixel Shield.  In this application we'd like to use it
// as a 5x8 tall matrix, with the USB port positioned at the top of the
// Arduino.  When held that way, the first pixel is at the top right, and
// lines are arranged in columns, progressive order.  The shield uses
// 800 KHz (v2) pixels that expect GRB color data.
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(NEO_WIDTH, NEO_HEIGHT, NEO_PIN,
  NEO_MATRIX_TOP     + NEO_MATRIX_RIGHT +
  NEO_MATRIX_ROWS + NEO_MATRIX_PROGRESSIVE,
  NEO_GRB            + NEO_KHZ800);




const std::vector<uint16_t> colors = {
  matrix.Color(255, 0, 0),
  matrix.Color(0, 255, 0),
  matrix.Color(0, 0, 255),
  matrix.Color(64, 0, 64)
  };

  const std::vector<uint16_t> emergency_colors = {
  RED,
  BLACK,
  YELLOW
  };

  int emergency_index = 0;

  // Adding GO objects
  const std::vector<uint16_t> go_colors = {
  RED,
  BLACK
  };

  int go_index = 0;

  // UART Initialization
  const long BAUD_RATE = 115200;

void setup() {
  matrix.setFont(&SimpleFont5pt7b);
  matrix.begin();
  matrix.setTextWrap(false);
  matrix.setBrightness(40);
  matrix.setTextColor(colors[0]);

  // UART setup
  Serial1.begin(BAUD_RATE);

  Serial1.println("Serial Port Initialized...");


}

int x    = matrix.width();
int pass = 0;

const std::vector<String> commands = {
  {"S"},
  {"G"},
  {"R"},
  {"L"},
  {"E"},
  {"B"},
};

// These are the functions that will be called for each corresponding UART command
void command_stop();
void command_go();
void command_turn_right();
void command_turn_left();
void command_emergency();
void command_battery_level(String percentage);

void execute_command(char command);



/// A flag to check and make sure the data is old before processing
bool data_is_old = true;


String new_command;
String old_command;
char previous_command_value = 'S';

String text;


void loop() {


  matrix.fillScreen(BLACK);
  matrix.setCursor(x, 4);

  if ( Serial1.available() ) {

    new_command = Serial1.readString(); // read the incoming data as string
    Serial1.println(new_command);

    char command_value = new_command[0];

    old_command = new_command;

    previous_command_value = command_value;

    data_is_old = false;

    execute_command(command_value);

  }

  else {

    new_command = old_command;

    data_is_old = true;

    execute_command(previous_command_value);
  }

}



void execute_command(char command) {

  switch ( command ) {

      case 'S' :
      command_stop();
      break;

      case 'G' :
      command_go();
      break;

      case 'R' :
      command_turn_right();
      break;

      case 'L' :
      command_turn_left();
      break;

      case 'E' :
      command_emergency();
      break;

      case 'B' :
      new_command.remove(0, 2);
      command_battery_level(new_command);
      break;

      default :
      Serial1.println(F("Error: Invalid Command!!!"));
      break;
    }

}


// These are the functions that will be called for each corresponding UART command
void command_stop() {

  text = "STOP";

  matrix.fillScreen(RED);
  matrix.setBrightness(255);
  matrix.show();
  delay(100);

}


void command_go() {

  text = "GO";



  if( ++go_index > ( go_colors.size() - 1 ) ) {

    go_index = 0;
  }


  matrix.fillScreen(go_colors[go_index]);
  matrix.setBrightness(128);
  matrix.show();
  delay(100);

}

void command_turn_right() {

  // matrix.print(F(">>>>"));

  text = ">>>>>>>>>>";

  // x    = -matrix.width();

  matrix.fillScreen(BLACK);
  matrix.setTextColor(RED);
  matrix.setCursor(x, 4);

  // Serial1.println(x);


  matrix.print(text);


  if(++x > 0) {
    x = -matrix.width();
    // Serial1.println(x);
    // if(++pass >= colors.size()) pass = 0;
    // matrix.setTextColor(RED);
  }
  matrix.show();
  delay(100);

}

void command_turn_left() {

  // matrix.print(F("<<<<"));

  text = "<<<<<<<<<<";

  // x    = matrix.width();

  matrix.fillScreen(BLACK);
  matrix.setTextColor(RED);
  matrix.setCursor(x, 4);

  // Serial1.println(x);


  matrix.print(text);


  if(--x < -20) {
    x = matrix.width();
    // Serial1.println(x);
    // if(++pass >= colors.size()) pass = 0;
    // matrix.setTextColor(RED);
  }
  matrix.show();
  delay(100);

}

void command_emergency() {

  // matrix.print(F("EMERGENCY!!!!"));

  text = "EMERGENCY!!!!";

  matrix.fillScreen(BLACK);
  matrix.setCursor(0, 0);

  if( ++emergency_index > ( emergency_colors.size() - 1 ) ) {

    emergency_index = 0;
  }


  matrix.drawTriangle(0, 5, 9, 0, 19, 5, emergency_colors[emergency_index]);
  matrix.fillTriangle(0, 5, 9, 0, 19, 5, emergency_colors[emergency_index]);

  matrix.show();
  delay(100);

}

void command_battery_level(String percentage) {

  unsigned int battery_level = percentage.toInt();

  if (battery_level < 0)
    battery_level = 0;

  else if (battery_level > 100)
    battery_level = 100;

  unsigned int bar_count = battery_level / 5;

  uint16_t battery_display_color;

  switch ( battery_level ) {

    case 0 ... 30:
    battery_display_color = RED;
    break;

    case 31 ... 60:
    battery_display_color = YELLOW;
    break;

    case 61 ... 100:
    battery_display_color = GREEN;
    break;

  }

  matrix.fillScreen(BLACK);
  matrix.setCursor(0, 0);


  matrix.drawRect(0,0, bar_count, 5, battery_display_color);
  matrix.fillRect(0,0, bar_count, 5, battery_display_color);

  matrix.show();
  delay(100);

}
