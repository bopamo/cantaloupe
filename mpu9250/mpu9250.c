

#include "mpu9250_register_map.h"
#include "mpu9250.h"



#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>


#include "gpio.h"
#include "twi.h"
#include "timer.h"
#include "ptpi_bsp.h"
#include "nrf_drv_twi.h"
#include "nrf_gpio.h"
#include "nrf_drv_gpiote.h"

#include "nrf_delay.h"

#include "app_error.h"
#include "sdk_errors.h"


#include "nrf_log.h"
#include "nrf_log_ctrl.h"



// Specify sensor full scale

uint8_t Gscale    = GFS_250DPS; // AFS_2G, AFS_4G, AFS_8G, AFS_16G
uint8_t Ascale    = AFS_2G;     // GFS_250DPS, GFS_500DPS, GFS_1000DPS, GFS_2000DPS
uint8_t Mscale    = MFS_16BITS; // Choose either 14-bit or 16-bit magnetometer resolution
uint8_t Mmode     = 0x06;        // 2 for 8 Hz, 6 for 100 Hz continuous magnetometer data read
uint8_t sRateDiv  = 0;
uint8_t Gyro_DLPF = GLPF_20HZ;

float aRes, gRes, mRes;      // scale resolutions per LSB for the sensors

// Pin definitions
int intPin = PTPI_MPU_INT_PIN;
bool newData = false;
bool newMagData = false;

// MPU Data
int16_t MPU9250Data[7]; // used to read all 14 bytes at once from the MPU9250 accel/gyro
int16_t accelCount[3];  // Stores the 16-bit signed accelerometer sensor output
int16_t gyroCount[3];   // Stores the 16-bit signed gyro sensor output
int16_t magCount[3];    // Stores the 16-bit signed magnetometer sensor output
float magCalibration[3] = {0, 0, 0};  // Factory mag calibration and mag bias
float gyroBias[3] = {0, 0, 0}, accelBias[3] = {0, 0, 0}, magBias[3] = {0, 0, 0}, magScale[3]  = {0, 0, 0};      // Bias corrections for gyro and accelerometer
int16_t tempCount;            // temperature raw count output
float temperature;            // Stores the MPU9250 gyro internal chip temperature in degrees Celsius
float SelfTest[6];            // holds results of gyro and accelerometer self test

// global constants for 9 DoF fusion and AHRS (Attitude and Heading Reference System)
#define PI 3.14159265358979323846f
#define GyroMeasError ( PI * (4.0f / 180.0f)  )   // gyroscope measurement error in rads/s (start at 40 deg/s)
#define GyroMeasDrift ( PI * (0.0f  / 180.0f) )   // gyroscope measurement drift in rad/s/s (start at 0.0 deg/s/s)
// There is a tradeoff in the beta parameter between accuracy and response speed.
// In the original Madgwick study, beta of 0.041 (corresponding to GyroMeasError of 2.7 degrees/s) was found to give optimal accuracy.
// However, with this value, the LSM9SD0 response time is about 10 seconds to a stable initial quaternion.
// Subsequent changes also require a longish lag time to a stable output, not fast enough for a quadcopter or robot car!
// By increasing beta (GyroMeasError) by about a factor of fifteen, the response time constant is reduced to ~2 sec
// I haven't noticed any reduction in solution accuracy. This is essentially the I coefficient in a PID control sense;
// the bigger the feedback coefficient, the faster the solution converges, usually at the expense of accuracy.
// In any case, this is the free parameter in the Madgwick filtering and fusion scheme.
#define beta sqrt(3.0f / 4.0f) * GyroMeasError   // compute beta
#define zeta sqrt(3.0f / 4.0f) * GyroMeasDrift   // compute zeta, the other free parameter in the Madgwick scheme usually set to a small or zero value
#define Kp 2.0f * 5.0f // these are the free parameters in the Mahony filter and fusion scheme, Kp for proportional feedback, Ki for integral
#define Ki 0.0f

uint32_t delt_t = 0, count = 0, sumCount = 0;  // used to control display output rate
float pitch, yaw, roll;
float a12, a22, a31, a32, a33;            // rotation matrix coefficients for Euler angles and gravity components
float deltat = 0.0f, sum = 0.0f;          // integration interval for both filter schemes
uint32_t lastUpdate = 0, firstUpdate = 0; // used to calculate integration interval
uint32_t Now = 0;                         // used to calculate integration interval

float ax, ay, az, gx, gy, gz, mx, my, mz; // variables to hold latest sensor data values
float lin_ax, lin_ay, lin_az;             // linear acceleration (acceleration with gravity component subtracted)
float q[4] = {1.0f, 0.0f, 0.0f, 0.0f};    // vector to hold quaternion
float eInt[3] = {0.0f, 0.0f, 0.0f};       // vector to hold integral error for Mahony method


#define DEGREES_TO_RADIANS ( PI * (1.0f  / 180.0f) )   // gyroscope measurement drift in rad/s/s (start at 0.0 deg/s/s)


float gFS_range;
float aFS_range;
float mFS_range; // TODO: This needs to be set


float gyro_sensitivity  = 0;    // = 131 LSB/degrees/sec
float accel_sensitivity = 0;  // = 16384 LSB/g




// Pin definitions
// int intPin = PTPI_MPU_INT_PIN;  // These can be changed, 2 and 3 are the Arduinos ext int pins


static mpu_config_t mpu9250_settings = {0};

static temp_value_t temp_isr;

static accel_values_t accel_isr;

static gyro_values_t gyro_isr;



static void mpu9250_get_Ares(void);

static void mpu9250_get_Gres(void);

static void mpu9250_get_Mres(void);



static uint32_t mpu9250_write_byte(uint8_t mpu9250_address, uint8_t mpu9250_register, uint8_t data);


static char mpu9250_read_single_byte(uint8_t mpu9250_address, uint8_t mpu9250_register);


static uint32_t mpu9250_read_multiple_bytes(uint8_t mpu9250_address, uint8_t mpu9250_register, uint8_t length, uint8_t * p_data);

static uint32_t mpu9250_read_whami(uint8_t * whoami_value);


/**
 * @brief: Simple interrupt handler setting a flag indicating that data is ready
 *
 */
void mpu9250_int_pin_handler(void)
{
        mpu9250_data_ready = true;

//        NRF_LOG_DEBUG("\r\nMPU9250 : ISR\r\n");


}


void mpu9250_timer_int_handler(void * p_context)
{

        // uint32_t err_code;
        // err_code = mpu9250_get_data((int16_t*)(&accel_isr), (int16_t*)(&gyro_isr), (int16_t*)(&temp_isr));
        // APP_ERROR_CHECK(err_code);
        mpu9250_get_raw_data_from_isr = true;

}


uint32_t mpu9250_clear_interrupts(void) {

        uint32_t err_code;

        uint8_t int_status = mpu9250_read_single_byte(MPU9250_ADDRESS, INT_STATUS);

        // NRF_LOG_DEBUG("INT STATUS = %d\n\r", int_status);

        return NRF_SUCCESS;


}

uint32_t mpu9250_wakeup(void) {

        DEBUG_FUNCTION_ENTER

        uint32_t err_code;

        // Initialize MPU9250 device
        // wake up device
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, PWR_MGMT_1, 0x00); // Clear sleep mode bit (6), enable all sensors
        APP_ERROR_CHECK(err_code);
        nrf_delay_ms(100); // Delay 100 ms for PLL to get established on x-axis gyro; should check for PLL ready interrupt

        // get stable time source
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, PWR_MGMT_1, 0x01); // Set clock source to be PLL with x-axis gyroscope reference, bits 2:0 = 001
        APP_ERROR_CHECK(err_code);


        DEBUG_FUNCTION_EXIT

        return err_code;

}

uint32_t mpu9250_whami(void) {

        uint32_t err_code;


        // Read the WHO_AM_I register, this is a good test of communication
        uint8_t whoami;
        err_code = mpu9250_read_whami(&whoami); // Read WHO_AM_I register for MPU-9250
        APP_ERROR_CHECK(err_code);
        // printf("I AM 0x%x\n\r", whoami); printf("I SHOULD BE %s\n\r", WHOAMI_MPU9250_VALUE);

        if (whoami == WHOAMI_MPU9250_VALUE) // WHO_AM_I should always be 0x68
        {
                printf("MPU9250 WHO_AM_I is 0x%x\n\r", whoami);
                printf("MPU9250 is online...\n\r");

        }

        else
        {
                printf("Could not connect to MPU9250: %#x \n\r", whoami);


                // printf("MPU9250", 0, 0);
                // printf("no connection", 0, 1);
                // sprintf(buffer, "WHO_AM_I 0x%x", whoami);
                // printf(buffer, 0, 2);

                while(1) ;   // Loop forever if communication doesn't happen
        }

        return err_code;

}

uint32_t mpu9250_whami_AK8963(void) {

        uint32_t err_code;


        // Read the WHO_AM_I register, this is a good test of communication
        uint8_t whoami = mpu9250_read_single_byte(AK8963_ADDRESS, WHO_AM_I_AK8963);  // Read WHO_AM_I register for AK8963
        // APP_ERROR_CHECK(err_code);
        // printf("I AM 0x%x\n\r", whoami); printf("I SHOULD BE %s\n\r", WHOAMI_MPU9250_VALUE);

        if (whoami == WHOAMI_AK8963_VALUE) // WHO_AM_I should always be 0x68
        {
                printf("MPU9250 WHO_AM_I_AK8963 is 0x%x\n\r", whoami);
                printf("MPU9250 is online...\n\r");

        }

        else
        {
                printf("Could not connect to MPU9250: %#x \n\r", whoami);


                // printf("MPU9250", 0, 0);
                // printf("no connection", 0, 1);
                // sprintf(buffer, "WHO_AM_I 0x%x", whoami);
                // printf(buffer, 0, 2);

                while(1) ;   // Loop forever if communication doesn't happen
        }

        return err_code;

}





//===================================================================================================================
//====== Set of useful function to access acceleration, gyroscope, and temperature data
//===================================================================================================================
uint32_t mpu9250_setup(void)
{

        DEBUG_FUNCTION_ENTER

        uint32_t err_code;


        Ascale    = AFS_2G;       // AFS_2G, AFS_4G, AFS_8G, AFS_16G
        Gscale    = GFS_250DPS;   // GFS_250DPS, GFS_500DPS, GFS_1000DPS, GFS_2000DPS
        Mscale    = MFS_16BITS;   // MFS_14BITS or MFS_16BITS, 14-bit or 16-bit magnetometer resolution
        Mmode     = 0x06;         // Either 8 Hz 0x02) or 100 Hz (0x06) magnetometer data ODR
        sRateDiv  = 0;            // Set sample rate = gyroscope output rate/(1 + SMPLRT_DIV)
        Gyro_DLPF = GLPF_10HZ;    // Bandwidth for Gyro DLPF

        // Wakeup
        err_code = mpu9250_wakeup();
        APP_ERROR_CHECK(err_code);

        // WHOAMI
        err_code = mpu9250_whami();
        APP_ERROR_CHECK(err_code);

        // Reset
        err_code = mpu9250_reset(); // Reset registers to default in preparation for device calibration
        APP_ERROR_CHECK(err_code);


        // Self-test
        err_code = mpu9250_self_test(SelfTest); // Start by performing self test and reporting values
        APP_ERROR_CHECK(err_code);

        printf("x-axis self test: acceleration trim within : %f % of factory value\n\r", SelfTest[0]);
        printf("y-axis self test: acceleration trim within : %f % of factory value\n\r", SelfTest[1]);
        printf("z-axis self test: acceleration trim within : %f % of factory value\n\r", SelfTest[2]);
        printf("\n\r");
        printf("x-axis self test: gyration trim within     : %f % of factory value\n\r", SelfTest[3]);
        printf("y-axis self test: gyration trim within     : %f % of factory value\n\r", SelfTest[4]);
        printf("z-axis self test: gyration trim within     : %f % of factory value\n\r", SelfTest[5]);
        printf("\n\r");


        // get sensor resolutions, only need to do this once
        mpu9250_get_Ares(); // Get accelerometer sensitivity
        mpu9250_get_Gres(); // Get gyro sensitivity
        mpu9250_get_Mres(); // Get magnetometer sensitivity

        // Calibrate
        printf("Calibrate Gyro and Accel\n\r");
        err_code = mpu9250_calibrate_gyro_accel(gyroBias, accelBias); // Calibrate gyro and accelerometers, load biases in bias registers
        APP_ERROR_CHECK(err_code);

        printf("x accel bias (mG)  = %f\n\r", 1000*accelBias[0]);
        printf("y accel bias (mG)  = %f\n\r", 1000*accelBias[1]);
        printf("z accel bias (mG)  = %f\n\r", 1000*accelBias[2]);
        printf("\n\r");
        printf("x gyro bias (dps) = %f\n\r", gyroBias[0]);
        printf("y gyro bias (dps) = %f\n\r", gyroBias[1]);
        printf("z gyro bias (dps) = %f\n\r", gyroBias[2]);
        printf("\n\r");



        // Init Gyro/Accel
        err_code = mpu9250_init();
        APP_ERROR_CHECK(err_code);


        // Initialize device for active mode read of acclerometer, gyroscope, and temperature
        printf("MPU9250 initialized for active data mode....\n\r");

        // Show config
        mpu9250_get_settings();


        // Read the WHO_AM_I register of the magnetometer, this is a good test of communication
        mpu9250_whami_AK8963();

        // Init Magnetometer
        err_code = mpu9250_init_ak8963(magCalibration);
        APP_ERROR_CHECK(err_code);

        printf("AK8963 initialized for active data mode....\n\r"); // Initialize device for active mode read of magnetometer

        // Calibrate Magnetometer
        printf("Calibrate Magnetometer\n\r");
//        err_code = mpu9250_calibrate_mag(magBias, magScale);
//        APP_ERROR_CHECK(err_code);

        printf("x mag bias (mG) = %f\n\r", magBias[0]);
        printf("y mag bias (mG) = %f\n\r", magBias[1]);
        printf("z mag bias (mG) = %f\n\r", magBias[2]);
        printf("\n\r");
        printf("x mag scale (mG) = %f\n\r", magScale[0]);
        printf("y mag scale (mG) = %f\n\r", magScale[1]);
        printf("z mag scale (mG) = %f\n\r", magScale[2]);
        printf("\n\r");
        printf("X-Axis sensitivity adjustment value %f\n\r", magCalibration[0]);
        printf("Y-Axis sensitivity adjustment value %f\n\r", magCalibration[1]);
        printf("Z-Axis sensitivity adjustment value %f\n\r", magCalibration[2]);
        printf("\n\r");


        printf("Accelerometer full-scale range = %f  g\n\r",  2.0f  * (float)(1<<Ascale));
        printf("Gyroscope full-scale range = %f  deg/s\n\r", 250.0f * (float)(1<<Gscale));
        if(Mscale == 0) printf("Magnetometer resolution = 14  bits\n\r");
        if(Mscale == 1) printf("Magnetometer resolution = 16  bits\n\r");
        if(Mmode == 2) printf("Magnetometer ODR = 8 Hz\n\r");
        if(Mmode == 6) printf("Magnetometer ODR = 100 Hz\n\r");



        // err_code = gpio_interrupts_init();
        // APP_ERROR_CHECK(err_code);




        DEBUG_FUNCTION_EXIT

        return err_code;

}

uint32_t mpu9250_init(void)
{

        DEBUG_FUNCTION_ENTER

        uint32_t err_code;

        // wake up device
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, PWR_MGMT_1, 0x00); // Clear sleep mode bit (6), enable all sensors
        APP_ERROR_CHECK(err_code);
        nrf_delay_ms(100); // Wait for all registers to reset

        // get stable time source
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, PWR_MGMT_1, 0x01); // Auto select clock source to be PLL gyroscope reference if ready else
        APP_ERROR_CHECK(err_code);
        nrf_delay_ms(200);

        // Configure Gyro and Thermometer
        // Disable FSYNC and set thermometer and gyro bandwidth to 41 and 42 Hz, respectively;
        // minimum delay time for this setting is 5.9 ms, which means sensor fusion update rates cannot
        // be higher than 1 / 0.0059 = 170 Hz
        // DLPF_CFG = bits 2:0 = 011; this limits the sample rate to 1000 Hz for both
        // With the MPU9250, it is possible to get gyro sample rates of 32 kHz (!), 8 kHz, or 1 kHz
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, REG_CONFIG, Gyro_DLPF);
        APP_ERROR_CHECK(err_code);

        // Set sample rate = gyroscope output rate/(1 + SMPLRT_DIV)
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, SMPLRT_DIV, sRateDiv); // Use a 200 Hz rate; a rate consistent with the filter update rate
        APP_ERROR_CHECK(err_code);
        // determined inset in CONFIG above

        // Set gyroscope full scale range
        // Range selects FS_SEL and AFS_SEL are 0 - 3, so 2-bit values are left-shifted into positions 4:3
        uint8_t c = mpu9250_read_single_byte(MPU9250_ADDRESS, GYRO_CONFIG); // get current GYRO_CONFIG register value
        // c = c & ~0xE0; // Clear self-test bits [7:5]
        c = c & ~0x02; // Clear Fchoice bits [1:0]
        c = c & ~0x18; // Clear AFS bits [4:3]
        c = c | Gscale << 3; // Set full scale range for the gyro
        // c = c | 0x00; // Set Fchoice for the gyro to 11 by writing its inverse to bits 1:0 of GYRO_CONFIG
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, GYRO_CONFIG, c ); // Write new GYRO_CONFIG value to register
        APP_ERROR_CHECK(err_code);

        // Set accelerometer full-scale range configuration
        c = mpu9250_read_single_byte(MPU9250_ADDRESS, ACCEL_CONFIG); // get current ACCEL_CONFIG register value
        // c = c & ~0xE0; // Clear self-test bits [7:5]
        c = c & ~0x18; // Clear AFS bits [4:3]
        c = c | Ascale << 3; // Set full scale range for the accelerometer
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, ACCEL_CONFIG, c); // Write new ACCEL_CONFIG register value
        APP_ERROR_CHECK(err_code);

        // Set accelerometer sample rate configuration
        // It is possible to get a 4 kHz sample rate from the accelerometer by choosing 1 for
        // accel_fchoice_b bit [3]; in this case the bandwidth is 1.13 kHz
        c = mpu9250_read_single_byte(MPU9250_ADDRESS, ACCEL_CONFIG2); // get current ACCEL_CONFIG2 register value
        c = c & ~0x0F; // Clear accel_fchoice_b (bit 3) and A_DLPFG (bits [2:0])
        c = c | 0x03; // Set accelerometer rate to 1 kHz and bandwidth to 41 Hz
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, ACCEL_CONFIG2, c); // Write new ACCEL_CONFIG2 register value
        APP_ERROR_CHECK(err_code);

        // The accelerometer, gyro, and thermometer are set to 1 kHz sample rates,
        // but all these rates are further reduced by a factor of 5 to 200 Hz because of the SMPLRT_DIV setting

        // Configure Interrupts and Bypass Enable
        // Set interrupt pin active high, push-pull, hold interrupt pin level HIGH until interrupt cleared,
        // clear on read of INT_STATUS, and enable I2C_BYPASS_EN so additional chips
        // can join the I2C bus and all can be controlled by the Arduino as master
        //   writeByte(MPU9250_ADDRESS, INT_PIN_CFG, 0x22);
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, INT_PIN_CFG, 0x22); // INT is 50 microsecond pulse and any read to clear
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, INT_ENABLE, 0x01); // Enable data ready (bit 0) interrupt
        APP_ERROR_CHECK(err_code);

        nrf_delay_ms(100);

        NRF_LOG_INFO("MPU9250 INITIALIZED...\r\n");
        NRF_LOG_FLUSH();

        DEBUG_FUNCTION_EXIT

        return err_code;
}



static uint32_t mpu9250_write_byte(uint8_t mpu9250_address, uint8_t mpu9250_register, uint8_t data)
{
        // char data_write[2];
        // data_write[0] = subAddress;
        // data_write[1] = data;
        // i2c.write(address, data_write, 2, 0);

        uint32_t err_code;

        err_code = twi_write_register(mpu9250_address, mpu9250_register, data);
        APP_ERROR_CHECK(err_code);

        return err_code;
}

static char mpu9250_read_single_byte(uint8_t mpu9250_address, uint8_t mpu9250_register)
{
        // char data[1]; // `data` will store the register data
        // char data_write[1];
        // data_write[0] = subAddress;
        // i2c.write(address, data_write, 1, 1); // no stop
        // i2c.read(address, data, 1, 0);
        // return data[0];

        uint8_t data[1];

        uint32_t err_code;
        err_code = twi_read_registers(mpu9250_address, mpu9250_register, data, 1);
        APP_ERROR_CHECK(err_code);

        return data[0];
}

static uint32_t mpu9250_read_multiple_bytes(uint8_t mpu9250_address, uint8_t mpu9250_register, uint8_t length, uint8_t * p_data)
{
        // char data[14];
        // char data_write[1];
        // data_write[0] = subAddress;
        // i2c.write(address, data_write, 1, 1); // no stop
        // i2c.read(address, data, count, 0);
        // for(int ii = 0; ii < count; ii++) {
        //         dest[ii] = data[ii];
        // }

        uint32_t err_code;
        err_code = twi_read_registers(mpu9250_address, mpu9250_register, p_data, length);
        APP_ERROR_CHECK(err_code);

        return err_code;
}



/**@brief Function for reading MPU WHOAMI.
 *
 * @param[in]   whoami_value     Pointer to variable to hold whoami data
 * @retval      uint32_t        Error code
 */
uint32_t mpu9250_read_whami(uint8_t * whoami_value)
{

        uint32_t err_code;
        uint8_t raw_values[1];
        raw_values[0] = mpu9250_read_single_byte(MPU9250_ADDRESS, WHO_AM_I_MPU9250);

        whoami_value[0] = (uint8_t)raw_values[0];

        return NRF_SUCCESS;
}


static void mpu9250_get_Mres(void) {

// Possible magnetometer scales (and their register bit settings) are:
// 14 bit resolution (0) and 16 bit resolution (1)

        switch (Mscale)
        {

        case MFS_14BITS:
                mRes = 10.0*4219.0/8190.0; // Proper scale to return milliGauss
                break;
        case MFS_16BITS:
                mRes = 10.0*4219.0/32760.0; // Proper scale to return milliGauss
                break;
        }
}


static void mpu9250_get_Gres(void) {

// Possible gyro scales (and their register bit settings) are:
// 250 DPS (00), 500 DPS (01), 1000 DPS (10), and 2000 DPS  (11).
// Here's a bit of an algorith to calculate DPS/(ADC tick) based on that 2-bit value:

        switch (Gscale)
        {

        case GFS_250DPS:
                gFS_range         = (250.0f);
                gyro_sensitivity  = (131.0f);
                break;
        case GFS_500DPS:
                gFS_range         = (500.0f);
                gyro_sensitivity  = (65.5f);
                break;
        case GFS_1000DPS:
                gFS_range         = (1000.0f);
                gyro_sensitivity  = (32.8f);
                break;
        case GFS_2000DPS:
                gFS_range         = (2000.0f);
                gyro_sensitivity  = (16.4f);
                break;
        }

        gRes = gFS_range/32768.0;
}


static void mpu9250_get_Ares(void) {

// Possible accelerometer scales (and their register bit settings) are:
// 2 Gs (00), 4 Gs (01), 8 Gs (10), and 16 Gs  (11).
// Here's a bit of an algorith to calculate DPS/(ADC tick) based on that 2-bit value:

        switch (Ascale)
        {

        case AFS_2G:
                aFS_range         = (2.0f);
                accel_sensitivity = 16384;
                break;
        case AFS_4G:
                aFS_range         = (4.0f);
                accel_sensitivity = 8192;
                break;
        case AFS_8G:
                aFS_range         = (8.0f);
                accel_sensitivity = 4096;
                break;
        case AFS_16G:
                aFS_range         = (16.0f);
                accel_sensitivity = 2048;
                break;
        }

        aRes = aFS_range/32768.0;
}


uint32_t mpu9250_read_accel(int16_t * destination)
{
        uint32_t err_code;
        uint8_t rawData[6]; // x/y/z accel register data stored here
        err_code = mpu9250_read_multiple_bytes(MPU9250_ADDRESS, ACCEL_XOUT_H, 6, &rawData[0]); // Read the six raw data registers into data array
        APP_ERROR_CHECK(err_code);
        destination[0] = (int16_t)(((int16_t)rawData[0] << 8) | rawData[1]); // Turn the MSB and LSB into a signed 16-bit value
        destination[1] = (int16_t)(((int16_t)rawData[2] << 8) | rawData[3]);
        destination[2] = (int16_t)(((int16_t)rawData[4] << 8) | rawData[5]);

        return err_code;
}

uint32_t mpu9250_read_gyro(int16_t * destination)
{
        uint32_t err_code;
        uint8_t rawData[6]; // x/y/z gyro register data stored here
        err_code = mpu9250_read_multiple_bytes(MPU9250_ADDRESS, GYRO_XOUT_H, 6, &rawData[0]); // Read the six raw data registers sequentially into data array
        APP_ERROR_CHECK(err_code);
        destination[0] = (int16_t)(((int16_t)rawData[0] << 8) | rawData[1]); // Turn the MSB and LSB into a signed 16-bit value
        destination[1] = (int16_t)(((int16_t)rawData[2] << 8) | rawData[3]);
        destination[2] = (int16_t)(((int16_t)rawData[4] << 8) | rawData[5]);

        return err_code;
}

uint32_t mpu9250_read_mag(int16_t * destination)
{
        uint32_t err_code;

        bool newMagData;


        uint8_t rawData[7]; // x/y/z gyro register data, ST2 register stored here, must read ST2 at end of data acquisition
        newMagData = ( mpu9250_read_single_byte(AK8963_ADDRESS, AK8963_ST1) & 0x01 );

        if(newMagData == true)
        { // wait for magnetometer data ready bit to be set
                err_code = mpu9250_read_multiple_bytes(AK8963_ADDRESS, AK8963_XOUT_L, 7, &rawData[0]); // Read the six raw data and ST2 registers sequentially into data array
                APP_ERROR_CHECK(err_code);

                uint8_t c = rawData[6]; // End data read by reading ST2 register

                if(!(c & 0x08))
                { // Check if magnetic sensor overflow set, if not then report data
                        destination[0] = (int16_t)(((int16_t)rawData[1] << 8) | rawData[0]); // Turn the MSB and LSB into a signed 16-bit value
                        destination[1] = (int16_t)(((int16_t)rawData[3] << 8) | rawData[2]); // Data stored as little Endian
                        destination[2] = (int16_t)(((int16_t)rawData[5] << 8) | rawData[4]);
                }
        }

        return err_code;
}


void mpu9250_convert_raw_accel(accel_t * aOut, accel_values_t * aRaw) {

  // Now we'll calculate the accleration value into actual g's
     aOut->x = (float)aRaw->x*aRes - accelBias[0];  // get actual g value, this depends on scale being set
     aOut->y = (float)aRaw->y*aRes - accelBias[1];
     aOut->z = (float)aRaw->z*aRes - accelBias[2];

}


void mpu9250_convert_raw_gyro(gyro_t * gOut, gyro_values_t * gRaw) {


        // Calculate the gyro value into actual Radians per second
        gOut->x = (float)gRaw->x*gRes*DEGREES_TO_RADIANS; // get actual gyro value, this depends on scale being set
        gOut->y = (float)gRaw->y*gRes*DEGREES_TO_RADIANS;
        gOut->z = (float)gRaw->z*gRes*DEGREES_TO_RADIANS;

}



void mpu9250_convert_raw_magn(mag_t * mOut, mag_values_t  * mRaw) {


        // Calculate the magnetometer values in milliGauss
        // Include factory calibration per data sheet and user environmental corrections
        if(newMagData == true)
        {
        newMagData = false; // reset newMagData flag
        mOut->x = (float)mRaw->x*mRes*magCalibration[0] - magBias[0];  // get actual magnetometer value, this depends on scale being set
        mOut->y = (float)mRaw->y*mRes*magCalibration[1] - magBias[1];
        mOut->z = (float)mRaw->z*mRes*magCalibration[2] - magBias[2];

        mOut->x *= magScale[0];
        mOut->y *= magScale[1];
        mOut->z *= magScale[2];
      }

}



int16_t mpu9250_read_temp(void)
{

        uint32_t err_code;

        uint8_t rawData[2]; // x/y/z gyro register data stored here
        err_code = mpu9250_read_multiple_bytes(MPU9250_ADDRESS, TEMP_OUT_H, 2, &rawData[0]); // Read the two raw data registers sequentially into data array
        APP_ERROR_CHECK(err_code);
        return (int16_t)(((int16_t)rawData[0]) << 8 | rawData[1]); // Turn the MSB and LSB into a 16-bit value
}

uint32_t mpu9250_get_data(int16_t * accel, int16_t * gyro, int16_t * temp)
{

        uint32_t err_code;

        uint8_t rawData[14]; // x/y/z gyro register data stored here
        err_code = mpu9250_read_multiple_bytes(MPU9250_ADDRESS, ACCEL_XOUT_H, 14, &rawData[0]); // Read the two raw data registers sequentially into data array
        APP_ERROR_CHECK(err_code);

        accel[0] = ((int16_t)rawData[0] << 8) | rawData[1];   // Turn the MSB and LSB into a signed 16-bit value
        accel[1] = ((int16_t)rawData[2] << 8) | rawData[3];
        accel[2] = ((int16_t)rawData[4] << 8) | rawData[5];

        *temp    = ((int16_t)rawData[6] << 8) | rawData[7];

        gyro[0] = ((int16_t)rawData[8] << 8)  | rawData[9];
        gyro[1] = ((int16_t)rawData[10] << 8) | rawData[11];
        gyro[2] = ((int16_t)rawData[12] << 8) | rawData[13];

        return err_code;

}


uint32_t mpu9250_get_settings(void)
{

        DEBUG_FUNCTION_ENTER
        uint32_t err_code;
        // mpu_config_t config = {0};

        // uint8_t mpu_settings[5]; // x/y/z gyro register data stored here
        err_code = mpu9250_read_multiple_bytes(MPU9250_ADDRESS, SMPLRT_DIV, 5, (uint8_t*)(&mpu9250_settings)); // Read the two raw data registers sequentially into data array
        APP_ERROR_CHECK(err_code);

        // mpu9250_get_Ares(); // Get accelerometer sensitivity
        // mpu9250_get_Gres(); // Get gyro sensitivity
        // mpu9250_get_Mres(); // Get magnetometer sensitivity


        /*
           smplrt_div;         // Divider from the gyroscope output rate used to generate the Sample Rate for the MPU-9150. Sample Rate = Gyroscope Output Rate / (1 + SMPLRT_DIV)
           sync_dlpf_gonfig;   // Digital low pass fileter and external Frame Synchronization (FSYNC) configuration structure
           gyro_config;        // Gyro configuration structure
           accel_config;       // Accelerometer configuration structure
           accel_config_2;
         */

        // config.smplrt_div         = (config.smplrt_div)mpu_settings[0];
        // config.sync_dlpf_gonfig   = mpu_settings[1];
        // config.gyro_config        = mpu_settings[2];
        // config.accel_config       = mpu_settings[3];
        // config.accel_config_2     = mpu_settings[4];

        printf("SMPLRT_DIV        = %d\n\r\n\r", mpu9250_settings.smplrt_div);
        /*
            Sensitivity Scale Factor
         */
        printf("GYRO_FS_SEL       = %d\n\r", mpu9250_settings.gyro_config.fs_sel);
        printf("FCHOICE_B         = %d\n\r", mpu9250_settings.gyro_config.f_choice);
        printf("DLPF_CFG          = %d\n\r", mpu9250_settings.sync_dlpf_gonfig.dlpf_cfg);
        printf("GYRO FS RANGE     = %f\n\r", gFS_range);
        printf("GYRO SS FACTOR    = %f\n\r", gyro_sensitivity);

        printf("gRes              = %f\n\r\n\r", gRes);



        printf("ACCEL FS SEL      = %d\n\r", mpu9250_settings.accel_config.afs_sel);
        printf("ACCEL FCHOICE     = %d\n\r", mpu9250_settings.accel_config_2.accel_f_choice_b);
        printf("A_DLPFCFG         = %d\n\r", mpu9250_settings.accel_config_2.a_dlpf_cfg);
        printf("ACCEL FS RANGE    = %f\n\r", aFS_range);
        printf("ACCEL SS FACTOR   = %f\n\r", accel_sensitivity);

        printf("aRes              = %f\n\r\n\r", aRes);




        // mpu9250_get_Gres();
        // mpu9250_get_Ares();


        DEBUG_FUNCTION_EXIT
        return err_code;

}



uint32_t mpu9250_reset(void) {

        uint32_t err_code;
        // reset device
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, PWR_MGMT_1, 0x80); // Write a one to bit 7 reset bit; toggle reset device
        APP_ERROR_CHECK(err_code);
        nrf_delay_ms(100);

        return err_code;
}

uint32_t mpu9250_init_ak8963(float * destination)
{
        DEBUG_FUNCTION_ENTER

        uint32_t err_code;
        // First extract the factory calibration for each magnetometer axis
        uint8_t rawData[3]; // x/y/z gyro calibration data stored here
        err_code = mpu9250_write_byte(AK8963_ADDRESS, AK8963_CNTL, 0x00); // Power down magnetometer
        APP_ERROR_CHECK(err_code);
        nrf_delay_ms(10);

        err_code = mpu9250_write_byte(AK8963_ADDRESS, AK8963_CNTL, 0x0F); // Enter Fuse ROM access mode
        APP_ERROR_CHECK(err_code);
        nrf_delay_ms(10);

        err_code = mpu9250_read_multiple_bytes(AK8963_ADDRESS, AK8963_ASAX, 3, &rawData[0]); // Read the x-, y-, and z-axis calibration values
        APP_ERROR_CHECK(err_code);
        destination[0] =  (float)(rawData[0] - 128)/256.0f + 1.0f;// Return x-axis sensitivity adjustment values, etc.
        destination[1] =  (float)(rawData[1] - 128)/256.0f + 1.0f;
        destination[2] =  (float)(rawData[2] - 128)/256.0f + 1.0f;

        err_code = mpu9250_write_byte(AK8963_ADDRESS, AK8963_CNTL, 0x00); // Power down magnetometer
        APP_ERROR_CHECK(err_code);
        nrf_delay_ms(10);

        // Configure the magnetometer for continuous read and highest resolution
        // set Mscale bit 4 to 1 (0) to enable 16 (14) bit resolution in CNTL register,
        // and enable continuous mode data acquisition Mmode (bits [3:0]), 0010 for 8 Hz and 0110 for 100 Hz sample rates
        err_code = mpu9250_write_byte(AK8963_ADDRESS, AK8963_CNTL, Mscale << 4 | Mmode); // Set magnetometer data resolution and sample ODR
        APP_ERROR_CHECK(err_code);
        nrf_delay_ms(10);

        DEBUG_FUNCTION_EXIT

        return err_code;
}




void mpu9250_sensitivity(void) {

        // DEBUG_FUNCTION_ENTER

        mpu9250_get_Ares(); // Get accelerometer sensitivity
        mpu9250_get_Gres(); // Get gyro sensitivity
        mpu9250_get_Mres(); // Get magnetometer sensitivity

        printf("Accelerometer sensitivity is %f LSB/g \n\r", 1.0f/aRes);
        printf("Gyroscope sensitivity is %f LSB/deg/s \n\r", 1.0f/gRes);
        printf("Magnetometer sensitivity is %f LSB/G  \n\r", 1.0f/mRes);
        printf("\n\r\n\r\r");

        // DEBUG_FUNCTION_EXIT

}




uint32_t mpu9250_calibrate_mag(float * dest1, float * dest2) {

        DEBUG_FUNCTION_ENTER



        uint32_t err_code;

        uint16_t ii = 0, sample_count = 0;
        int32_t mag_bias[3] = {0, 0, 0}, mag_scale[3] = {0, 0, 0};
        int16_t mag_max[3] = {-32767, -32767, -32767}, mag_min[3] = {32767, 32767, 32767}, mag_temp[3] = {0, 0, 0};

        printf("Pre-Mag Calibration: \r\nx:\t%f\r\ny:\t%f\r\nz:\t%f\r\n", mag_bias[0], mag_bias[1], mag_bias[2]);

        printf("Mag Calibration: Wave device in a figure eight until done!\n");
        nrf_delay_ms(4000);

        // shoot for ~fifteen seconds of mag data
        if(Mmode == 0x02) sample_count = 128;      // at 8 Hz ODR, new mag data is available every 125 ms
        if(Mmode == 0x06) sample_count = 1500;      // at 100 Hz ODR, new mag data is available every 10 ms

        for(ii = 0; ii < sample_count; ii++)
        {
                err_code = mpu9250_read_mag(mag_temp); // Read the mag data
                APP_ERROR_CHECK(err_code);

                for (int jj = 0; jj < 3; jj++)
                {
                        if(mag_temp[jj] > mag_max[jj]) mag_max[jj] = mag_temp[jj];
                        if(mag_temp[jj] < mag_min[jj]) mag_min[jj] = mag_temp[jj];
                }

                if(Mmode == 0x02) nrf_delay_ms(135); // at 8 Hz ODR, new mag data is available every 125 ms
                if(Mmode == 0x06) nrf_delay_ms(12); // at 100 Hz ODR, new mag data is available every 10 ms
        }

        printf("mag x min/max: %d/%d",mag_max[0], mag_min[0]);
        printf("mag y min/max: %d/%d",mag_max[1], mag_min[1]);
        printf("mag z min/max: %d/%d",mag_max[2], mag_min[2]);


        // Get hard iron correction
        mag_bias[0]  = (mag_max[0] + mag_min[0])/2;      // get average x mag bias in counts
        mag_bias[1]  = (mag_max[1] + mag_min[1])/2;      // get average y mag bias in counts
        mag_bias[2]  = (mag_max[2] + mag_min[2])/2;      // get average z mag bias in counts

        dest1[0] = (float) mag_bias[0]*mRes*magCalibration[0];      // save mag biases in G for main program
        dest1[1] = (float) mag_bias[1]*mRes*magCalibration[1];
        dest1[2] = (float) mag_bias[2]*mRes*magCalibration[2];

        // Get soft iron correction estimate
        mag_scale[0]  = (mag_max[0] - mag_min[0])/2;      // get average x axis max chord length in counts
        mag_scale[1]  = (mag_max[1] - mag_min[1])/2;      // get average y axis max chord length in counts
        mag_scale[2]  = (mag_max[2] - mag_min[2])/2;      // get average z axis max chord length in counts

        float avg_rad = mag_scale[0] + mag_scale[1] + mag_scale[2];
        avg_rad /= 3.0;

        dest2[0] = avg_rad/((float)mag_scale[0]);
        dest2[1] = avg_rad/((float)mag_scale[1]);
        dest2[2] = avg_rad/((float)mag_scale[2]);

        printf("Post-Mag Calibration: \r\nx:\t%f\r\ny:\t%f\r\nz:\t%f\r\n", mag_bias[0], mag_bias[1], mag_bias[2]);
        printf("Mag Calibration done!\n");

        DEBUG_FUNCTION_EXIT
}    // end of calibrateMag






// Function which accumulates gyro and accelerometer data after device initialization. It calculates the average
// of the at-rest readings and then loads the resulting offsets into accelerometer and gyro bias registers.
uint32_t mpu9250_calibrate_gyro_accel(float * dest1, float * dest2)
{

        DEBUG_FUNCTION_ENTER

        uint32_t err_code;

        uint8_t data[12]; // data array to hold accelerometer and gyro x, y, z, data
        uint16_t ii, packet_count, fifo_count;
        int32_t gyro_bias[3]  = {0, 0, 0}, accel_bias[3] = {0, 0, 0};

        // reset device
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, PWR_MGMT_1, 0x80); // Write a one to bit 7 reset bit; toggle reset device
        APP_ERROR_CHECK(err_code);

        nrf_delay_ms(100);

        // get stable time source; Auto select clock source to be PLL gyroscope reference if ready
        // else use the internal oscillator, bits 2:0 = 001
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, PWR_MGMT_1, 0x01);
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, PWR_MGMT_2, 0x00);
        APP_ERROR_CHECK(err_code);

        nrf_delay_ms(200);

        // Configure device for bias calculation
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, INT_ENABLE, 0x00); // Disable all interrupts
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, FIFO_EN, 0x00); // Disable FIFO
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, PWR_MGMT_1, 0x00); // Turn on internal clock source
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, I2C_MST_CTRL, 0x00); // Disable I2C master
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, USER_CTRL, 0x00); // Disable FIFO and I2C master modes
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, USER_CTRL, 0x0C); // Reset FIFO and DMP
        APP_ERROR_CHECK(err_code);

        nrf_delay_ms(15);

        // Configure MPU6050 gyro and accelerometer for bias calculation
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, REG_CONFIG, 0x01); // Set low-pass filter to 188 Hz
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, SMPLRT_DIV, 0x00); // Set sample rate to 1 kHz
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, GYRO_CONFIG, 0x00); // Set gyro full-scale to 250 degrees per second, maximum sensitivity
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, ACCEL_CONFIG, 0x00); // Set accelerometer full-scale to 2 g, maximum sensitivity
        APP_ERROR_CHECK(err_code);


        uint16_t gyrosensitivity  = 131;// = 131 LSB/degrees/sec
        uint16_t accelsensitivity = 16384; // = 16384 LSB/g

        // Configure FIFO to capture accelerometer and gyro data for bias calculation
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, USER_CTRL, 0x40); // Enable FIFO
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, FIFO_EN, 0x78); // Enable gyro and accelerometer sensors for FIFO  (max size 512 bytes in MPU-9150)
        APP_ERROR_CHECK(err_code);

        nrf_delay_ms(40); // accumulate 40 samples in 40 milliseconds = 480 bytes

        // At end of sample accumulation, turn off FIFO sensor read
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, FIFO_EN, 0x00);  // Disable gyro and accelerometer sensors for FIFO
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_read_multiple_bytes(MPU9250_ADDRESS, FIFO_COUNTH, 2, &data[0]); // read FIFO sample count
        APP_ERROR_CHECK(err_code);

        fifo_count = ((uint16_t)data[0] << 8) | data[1];
        packet_count = fifo_count/12;// How many sets of full gyro and accelerometer data for averaging

        for (ii = 0; ii < packet_count; ii++) {
                int16_t accel_temp[3] = {0, 0, 0}, gyro_temp[3] = {0, 0, 0};
                err_code = mpu9250_read_multiple_bytes(MPU9250_ADDRESS, FIFO_R_W, 12, &data[0]); // read data for averaging
                APP_ERROR_CHECK(err_code);

                accel_temp[0] = (int16_t) (((int16_t)data[0] << 8) | data[1]  );// Form signed 16-bit integer for each sample in FIFO
                accel_temp[1] = (int16_t) (((int16_t)data[2] << 8) | data[3]  );
                accel_temp[2] = (int16_t) (((int16_t)data[4] << 8) | data[5]  );
                gyro_temp[0]  = (int16_t) (((int16_t)data[6] << 8) | data[7]  );
                gyro_temp[1]  = (int16_t) (((int16_t)data[8] << 8) | data[9]  );
                gyro_temp[2]  = (int16_t) (((int16_t)data[10] << 8) | data[11]);

                accel_bias[0] += (int32_t) accel_temp[0]; // Sum individual signed 16-bit biases to get accumulated signed 32-bit biases
                accel_bias[1] += (int32_t) accel_temp[1];
                accel_bias[2] += (int32_t) accel_temp[2];
                gyro_bias[0]  += (int32_t) gyro_temp[0];
                gyro_bias[1]  += (int32_t) gyro_temp[1];
                gyro_bias[2]  += (int32_t) gyro_temp[2];

        }
        accel_bias[0] /= (int32_t) packet_count; // Normalize sums to get average count biases
        accel_bias[1] /= (int32_t) packet_count;
        accel_bias[2] /= (int32_t) packet_count;
        gyro_bias[0]  /= (int32_t) packet_count;
        gyro_bias[1]  /= (int32_t) packet_count;
        gyro_bias[2]  /= (int32_t) packet_count;

        if(accel_bias[2] > 0L) {accel_bias[2] -= (int32_t) accelsensitivity; } // Remove gravity from the z-axis accelerometer bias calculation
        else {accel_bias[2] += (int32_t) accelsensitivity; }

        // Construct the gyro biases for push to the hardware gyro bias registers, which are reset to zero upon device startup
        data[0] = (-gyro_bias[0]/4  >> 8) & 0xFF;// Divide by 4 to get 32.9 LSB per deg/s to conform to expected bias input format
        data[1] = (-gyro_bias[0]/4)       & 0xFF;// Biases are additive, so change sign on calculated average gyro biases
        data[2] = (-gyro_bias[1]/4  >> 8) & 0xFF;
        data[3] = (-gyro_bias[1]/4)       & 0xFF;
        data[4] = (-gyro_bias[2]/4  >> 8) & 0xFF;
        data[5] = (-gyro_bias[2]/4)       & 0xFF;

        // Push gyro biases to hardware registers
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, XG_OFFSET_H, data[0]);
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, XG_OFFSET_L, data[1]);
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, YG_OFFSET_H, data[2]);
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, YG_OFFSET_L, data[3]);
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, ZG_OFFSET_H, data[4]);
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, ZG_OFFSET_L, data[5]);
        APP_ERROR_CHECK(err_code);


        // Output scaled gyro biases for display in the main program
        dest1[0] = (float) gyro_bias[0]/(float) gyrosensitivity;
        dest1[1] = (float) gyro_bias[1]/(float) gyrosensitivity;
        dest1[2] = (float) gyro_bias[2]/(float) gyrosensitivity;

        // Construct the accelerometer biases for push to the hardware accelerometer bias registers. These registers contain
        // factory trim values which must be added to the calculated accelerometer biases; on boot up these registers will hold
        // non-zero values. In addition, bit 0 of the lower byte must be preserved since it is used for temperature
        // compensation calculations. Accelerometer bias registers expect bias input as 2048 LSB per g, so that
        // the accelerometer biases calculated above must be divided by 8.

        int32_t accel_bias_reg[3] = {0, 0, 0}; // A place to hold the factory accelerometer trim biases
        err_code =  mpu9250_read_multiple_bytes(MPU9250_ADDRESS, XA_OFFSET_H, 2, &data[0]); // Read factory accelerometer trim values
        APP_ERROR_CHECK(err_code);

        accel_bias_reg[0] = (int32_t) (((int16_t)data[0] << 8) | data[1]);

        err_code = mpu9250_read_multiple_bytes(MPU9250_ADDRESS, YA_OFFSET_H, 2, &data[0]);
        APP_ERROR_CHECK(err_code);

        accel_bias_reg[1] = (int32_t) (((int16_t)data[0] << 8) | data[1]);

        err_code = mpu9250_read_multiple_bytes(MPU9250_ADDRESS, ZA_OFFSET_H, 2, &data[0]);
        APP_ERROR_CHECK(err_code);

        accel_bias_reg[2] = (int32_t) (((int16_t)data[0] << 8) | data[1]);

        uint32_t mask = 1uL; // Define mask for temperature compensation bit 0 of lower byte of accelerometer bias registers
        uint8_t mask_bit[3] = {0, 0, 0}; // Define array to hold mask bit for each accelerometer bias axis

        for(ii = 0; ii < 3; ii++) {
                if((accel_bias_reg[ii] & mask)) mask_bit[ii] = 0x01; // If temperature compensation bit is set, record that fact in mask_bit
        }

        // Construct total accelerometer bias, including calculated average accelerometer bias from above
        accel_bias_reg[0] -= (accel_bias[0]/8); // Subtract calculated averaged accelerometer bias scaled to 2048 LSB/g (16 g full scale)
        accel_bias_reg[1] -= (accel_bias[1]/8);
        accel_bias_reg[2] -= (accel_bias[2]/8);

        data[0] = (accel_bias_reg[0] >> 8) & 0xFF;
        data[1] = (accel_bias_reg[0])      & 0xFF;
        data[1] = data[1] | mask_bit[0]; // preserve temperature compensation bit when writing back to accelerometer bias registers
        data[2] = (accel_bias_reg[1] >> 8) & 0xFF;
        data[3] = (accel_bias_reg[1])      & 0xFF;
        data[3] = data[3] | mask_bit[1]; // preserve temperature compensation bit when writing back to accelerometer bias registers
        data[4] = (accel_bias_reg[2] >> 8) & 0xFF;
        data[5] = (accel_bias_reg[2])      & 0xFF;
        data[5] = data[5] | mask_bit[2]; // preserve temperature compensation bit when writing back to accelerometer bias registers

// Apparently this is not working for the acceleration biases in the MPU-9250
// Are we handling the temperature correction bit properly?
// Push accelerometer biases to hardware registers
/*  mpu9250_write_byte(MPU9250_ADDRESS, XA_OFFSET_H, data[0]);
   mpu9250_write_byte(MPU9250_ADDRESS, XA_OFFSET_L, data[1]);
   mpu9250_write_byte(MPU9250_ADDRESS, YA_OFFSET_H, data[2]);
   mpu9250_write_byte(MPU9250_ADDRESS, YA_OFFSET_L, data[3]);
   mpu9250_write_byte(MPU9250_ADDRESS, ZA_OFFSET_H, data[4]);
   mpu9250_write_byte(MPU9250_ADDRESS, ZA_OFFSET_L, data[5]);
 */
// Output scaled accelerometer biases for display in the main program
        dest2[0] = (float)accel_bias[0]/(float)accelsensitivity;
        dest2[1] = (float)accel_bias[1]/(float)accelsensitivity;
        dest2[2] = (float)accel_bias[2]/(float)accelsensitivity;


        DEBUG_FUNCTION_EXIT

        return err_code;
}


// Accelerometer and gyroscope self test; check calibration wrt factory settings
uint32_t mpu9250_self_test(float * destination) // Should return percent deviation from factory trim values, +/- 14 or less deviation is a pass
{

        DEBUG_FUNCTION_ENTER

        uint32_t err_code;


        uint8_t rawData[6] = {0, 0, 0, 0, 0, 0};
        uint8_t selfTest[6];
        int32_t gAvg[3] = {0}, aAvg[3] = {0}, aSTAvg[3] = {0}, gSTAvg[3] = {0};
        float factoryTrim[6];
        uint8_t FS = 0;

        // Set gyro sample rate to 1 kHz
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, SMPLRT_DIV, 0x00);
        APP_ERROR_CHECK(err_code);

        // Set gyro sample rate to 1 kHz and DLPF to 92 Hz
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, REG_CONFIG, 0x02);
        APP_ERROR_CHECK(err_code);

        // Set full scale range for the gyro to 250 dps
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, GYRO_CONFIG, FS<<3);
        APP_ERROR_CHECK(err_code);

        // Set accelerometer rate to 1 kHz and bandwidth to 92 Hz
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, ACCEL_CONFIG2, 0x02); // Set accelerometer rate to 1 kHz and bandwidth to 92 Hz
        APP_ERROR_CHECK(err_code);

        // Set full scale range for the accelerometer to 2 g
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, ACCEL_CONFIG, FS<<3); // Set full scale range for the accelerometer to 2 g
        APP_ERROR_CHECK(err_code);

        for( int ii = 0; ii < 200; ii++) { // get average current values of gyro and acclerometer

                // Read the six raw data registers into data array
                err_code = mpu9250_read_multiple_bytes(MPU9250_ADDRESS, ACCEL_XOUT_H, 6, &rawData[0]);
                APP_ERROR_CHECK(err_code);

                // Turn the MSB and LSB into a signed 16-bit value
                aAvg[0] += (int16_t)(((int16_t)rawData[0] << 8) | rawData[1]);
                aAvg[1] += (int16_t)(((int16_t)rawData[2] << 8) | rawData[3]);
                aAvg[2] += (int16_t)(((int16_t)rawData[4] << 8) | rawData[5]);


                // Read the six raw data registers sequentially into data array
                err_code = mpu9250_read_multiple_bytes(MPU9250_ADDRESS, GYRO_XOUT_H, 6, &rawData[0]);
                APP_ERROR_CHECK(err_code);

                // Turn the MSB and LSB into a signed 16-bit value
                gAvg[0] += (int16_t)(((int16_t)rawData[0] << 8) | rawData[1]);
                gAvg[1] += (int16_t)(((int16_t)rawData[2] << 8) | rawData[3]);
                gAvg[2] += (int16_t)(((int16_t)rawData[4] << 8) | rawData[5]);
        }

        // Get average of 200 values and store as average current readings
        for (int ii =0; ii < 3; ii++) {
                aAvg[ii] /= 200;
                gAvg[ii] /= 200;
        }

        // Configure the accelerometer for self-test
        // ... Enable self test on all three axes and set accelerometer range to +/- 2 g
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, ACCEL_CONFIG, 0xE0);
        APP_ERROR_CHECK(err_code);

        // ... Enable self test on all three axes and set gyro range to +/- 250 degrees/s
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, GYRO_CONFIG,  0xE0);
        APP_ERROR_CHECK(err_code);
        nrf_delay_ms(25); // Delay a while to let the device stabilize

        for( int ii = 0; ii < 200; ii++) { // get average self-test values of gyro and acclerometer

                err_code = mpu9250_read_multiple_bytes(MPU9250_ADDRESS, ACCEL_XOUT_H, 6, &rawData[0]); // Read the six raw data registers into data array
                APP_ERROR_CHECK(err_code);

                aSTAvg[0] += (int16_t)(((int16_t)rawData[0] << 8) | rawData[1]); // Turn the MSB and LSB into a signed 16-bit value
                aSTAvg[1] += (int16_t)(((int16_t)rawData[2] << 8) | rawData[3]);
                aSTAvg[2] += (int16_t)(((int16_t)rawData[4] << 8) | rawData[5]);


                // Read the six raw data registers sequentially into data array
                err_code = mpu9250_read_multiple_bytes(MPU9250_ADDRESS, GYRO_XOUT_H, 6, &rawData[0]);
                APP_ERROR_CHECK(err_code);

                // Turn the MSB and LSB into a signed 16-bit value
                gSTAvg[0] += (int16_t)(((int16_t)rawData[0] << 8) | rawData[1]);
                gSTAvg[1] += (int16_t)(((int16_t)rawData[2] << 8) | rawData[3]);
                gSTAvg[2] += (int16_t)(((int16_t)rawData[4] << 8) | rawData[5]);
        }

        for (int ii =0; ii < 3; ii++) { // Get average of 200 values and store as average self-test readings
                aSTAvg[ii] /= 200;
                gSTAvg[ii] /= 200;
        }

        // Configure the gyro and accelerometer for normal operation
        err_code = mpu9250_write_byte(MPU9250_ADDRESS, ACCEL_CONFIG, 0x00);
        APP_ERROR_CHECK(err_code);

        err_code = mpu9250_write_byte(MPU9250_ADDRESS, GYRO_CONFIG,  0x00);
        APP_ERROR_CHECK(err_code);

        nrf_delay_ms(25); // Delay a while to let the device stabilize

        // Retrieve accelerometer and gyro factory Self-Test Code from USR_Reg
        selfTest[0] = mpu9250_read_single_byte(MPU9250_ADDRESS, SELF_TEST_X_ACCEL); // X-axis accel self-test results
        APP_ERROR_CHECK(err_code);

        selfTest[1] = mpu9250_read_single_byte(MPU9250_ADDRESS, SELF_TEST_Y_ACCEL); // Y-axis accel self-test results
        APP_ERROR_CHECK(err_code);

        selfTest[2] = mpu9250_read_single_byte(MPU9250_ADDRESS, SELF_TEST_Z_ACCEL); // Z-axis accel self-test results
        APP_ERROR_CHECK(err_code);

        selfTest[3] = mpu9250_read_single_byte(MPU9250_ADDRESS, SELF_TEST_X_GYRO); // X-axis gyro self-test results
        APP_ERROR_CHECK(err_code);

        selfTest[4] = mpu9250_read_single_byte(MPU9250_ADDRESS, SELF_TEST_Y_GYRO); // Y-axis gyro self-test results
        APP_ERROR_CHECK(err_code);

        selfTest[5] = mpu9250_read_single_byte(MPU9250_ADDRESS, SELF_TEST_Z_GYRO); // Z-axis gyro self-test results
        APP_ERROR_CHECK(err_code);


        // Retrieve factory self-test value from self-test code reads
        factoryTrim[0] = (float)(2620/1<<FS)*(pow( 1.01, ((float)selfTest[0] - 1.0) )); // FT[Xa] factory trim calculation
        factoryTrim[1] = (float)(2620/1<<FS)*(pow( 1.01, ((float)selfTest[1] - 1.0) )); // FT[Ya] factory trim calculation
        factoryTrim[2] = (float)(2620/1<<FS)*(pow( 1.01, ((float)selfTest[2] - 1.0) )); // FT[Za] factory trim calculation
        factoryTrim[3] = (float)(2620/1<<FS)*(pow( 1.01, ((float)selfTest[3] - 1.0) )); // FT[Xg] factory trim calculation
        factoryTrim[4] = (float)(2620/1<<FS)*(pow( 1.01, ((float)selfTest[4] - 1.0) )); // FT[Yg] factory trim calculation
        factoryTrim[5] = (float)(2620/1<<FS)*(pow( 1.01, ((float)selfTest[5] - 1.0) )); // FT[Zg] factory trim calculation

        // Report results as a ratio of (STR - FT)/FT; the change from Factory Trim of the Self-Test Response
        // To get percent, must multiply by 100
        for (int i = 0; i < 3; i++) {
                destination[i]   = 100.0*((float)(aSTAvg[i] - aAvg[i]))/factoryTrim[i] - 100.;// Report percent differences
                destination[i+3] = 100.0*((float)(gSTAvg[i] - gAvg[i]))/factoryTrim[i+3] - 100.; // Report percent differences
        }

        DEBUG_FUNCTION_EXIT

        return err_code;

}



// Implementation of Sebastian Madgwick's "...efficient orientation filter for... inertial/magnetic sensor arrays"
// (see http://www.x-io.co.uk/category/open-source/ for examples and more details)
// which fuses acceleration, rotation rate, and magnetic moments to produce a quaternion-based estimate of absolute
// device orientation -- which can be converted to yaw, pitch, and roll. Useful for stabilizing quadcopters, etc.
// The performance of the orientation filter is at least as good as conventional Kalman-based filtering algorithms
// but is much less computationally intensive---it can be performed on a 3.3 V Pro Mini operating at 8 MHz!
void MadgwickQuaternionUpdate(float ax, float ay, float az, float gx, float gy, float gz, float mx, float my, float mz)
{
        float q1 = q[0], q2 = q[1], q3 = q[2], q4 = q[3];       // short name local variable for readability
        float norm;
        float hx, hy, _2bx, _2bz;
        float s1, s2, s3, s4;
        float qDot1, qDot2, qDot3, qDot4;

        // Auxiliary variables to avoid repeated arithmetic
        float _2q1mx;
        float _2q1my;
        float _2q1mz;
        float _2q2mx;
        float _4bx;
        float _4bz;
        float _2q1 = 2.0f * q1;
        float _2q2 = 2.0f * q2;
        float _2q3 = 2.0f * q3;
        float _2q4 = 2.0f * q4;
        float _2q1q3 = 2.0f * q1 * q3;
        float _2q3q4 = 2.0f * q3 * q4;
        float q1q1 = q1 * q1;
        float q1q2 = q1 * q2;
        float q1q3 = q1 * q3;
        float q1q4 = q1 * q4;
        float q2q2 = q2 * q2;
        float q2q3 = q2 * q3;
        float q2q4 = q2 * q4;
        float q3q3 = q3 * q3;
        float q3q4 = q3 * q4;
        float q4q4 = q4 * q4;

        // Normalise accelerometer measurement
        norm = sqrt(ax * ax + ay * ay + az * az);
        if (norm == 0.0f) return;     // handle NaN
        norm = 1.0f/norm;
        ax *= norm;
        ay *= norm;
        az *= norm;

        // Normalise magnetometer measurement
        norm = sqrt(mx * mx + my * my + mz * mz);
        if (norm == 0.0f) return;     // handle NaN
        norm = 1.0f/norm;
        mx *= norm;
        my *= norm;
        mz *= norm;

        // Reference direction of Earth's magnetic field
        _2q1mx = 2.0f * q1 * mx;
        _2q1my = 2.0f * q1 * my;
        _2q1mz = 2.0f * q1 * mz;
        _2q2mx = 2.0f * q2 * mx;
        hx = mx * q1q1 - _2q1my * q4 + _2q1mz * q3 + mx * q2q2 + _2q2 * my * q3 + _2q2 * mz * q4 - mx * q3q3 - mx * q4q4;
        hy = _2q1mx * q4 + my * q1q1 - _2q1mz * q2 + _2q2mx * q3 - my * q2q2 + my * q3q3 + _2q3 * mz * q4 - my * q4q4;
        _2bx = sqrt(hx * hx + hy * hy);
        _2bz = -_2q1mx * q3 + _2q1my * q2 + mz * q1q1 + _2q2mx * q4 - mz * q2q2 + _2q3 * my * q4 - mz * q3q3 + mz * q4q4;
        _4bx = 2.0f * _2bx;
        _4bz = 2.0f * _2bz;

        // Gradient decent algorithm corrective step
        s1 = -_2q3 * (2.0f * q2q4 - _2q1q3 - ax) + _2q2 * (2.0f * q1q2 + _2q3q4 - ay) - _2bz * q3 * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (-_2bx * q4 + _2bz * q2) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + _2bx * q3 * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
        s2 = _2q4 * (2.0f * q2q4 - _2q1q3 - ax) + _2q1 * (2.0f * q1q2 + _2q3q4 - ay) - 4.0f * q2 * (1.0f - 2.0f * q2q2 - 2.0f * q3q3 - az) + _2bz * q4 * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (_2bx * q3 + _2bz * q1) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + (_2bx * q4 - _4bz * q2) * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
        s3 = -_2q1 * (2.0f * q2q4 - _2q1q3 - ax) + _2q4 * (2.0f * q1q2 + _2q3q4 - ay) - 4.0f * q3 * (1.0f - 2.0f * q2q2 - 2.0f * q3q3 - az) + (-_4bx * q3 - _2bz * q1) * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (_2bx * q2 + _2bz * q4) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + (_2bx * q1 - _4bz * q3) * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
        s4 = _2q2 * (2.0f * q2q4 - _2q1q3 - ax) + _2q3 * (2.0f * q1q2 + _2q3q4 - ay) + (-_4bx * q4 + _2bz * q2) * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (-_2bx * q1 + _2bz * q3) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + _2bx * q2 * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
        norm = sqrt(s1 * s1 + s2 * s2 + s3 * s3 + s4 * s4);        // normalise step magnitude
        norm = 1.0f/norm;
        s1 *= norm;
        s2 *= norm;
        s3 *= norm;
        s4 *= norm;

        // Compute rate of change of quaternion
        qDot1 = (0.5f * (-q2 * gx - q3 * gy - q4 * gz) - beta * s1);
        qDot2 = (0.5f * (q1 * gx + q3 * gz - q4 * gy)  - beta * s2);
        qDot3 = (0.5f * (q1 * gy - q2 * gz + q4 * gx)  - beta * s3);
        qDot4 = (0.5f * (q1 * gz + q2 * gy - q3 * gx)  - beta * s4);

        // Integrate to yield quaternion
        q1 += qDot1 * deltat;
        q2 += qDot2 * deltat;
        q3 += qDot3 * deltat;
        q4 += qDot4 * deltat;
        norm = sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);        // normalise quaternion
        norm = 1.0f/norm;
        q[0] = q1 * norm;
        q[1] = q2 * norm;
        q[2] = q3 * norm;
        q[3] = q4 * norm;

}



// Similar to Madgwick scheme but uses proportional and integral filtering on the error between estimated reference vectors and
// measured ones.
void MahonyQuaternionUpdate(float ax, float ay, float az, float gx, float gy, float gz, float mx, float my, float mz)
{
        float q1 = q[0], q2 = q[1], q3 = q[2], q4 = q[3];       // short name local variable for readability
        float norm;
        float hx, hy, bx, bz;
        float vx, vy, vz, wx, wy, wz;
        float ex, ey, ez;
        float pa, pb, pc;

        // Auxiliary variables to avoid repeated arithmetic
        float q1q1 = q1 * q1;
        float q1q2 = q1 * q2;
        float q1q3 = q1 * q3;
        float q1q4 = q1 * q4;
        float q2q2 = q2 * q2;
        float q2q3 = q2 * q3;
        float q2q4 = q2 * q4;
        float q3q3 = q3 * q3;
        float q3q4 = q3 * q4;
        float q4q4 = q4 * q4;

        // Normalise accelerometer measurement
        norm = sqrt(ax * ax + ay * ay + az * az);
        if (norm == 0.0f) return;     // handle NaN
        norm = 1.0f / norm;            // use reciprocal for division
        ax *= norm;
        ay *= norm;
        az *= norm;

        // Normalise magnetometer measurement
        norm = sqrt(mx * mx + my * my + mz * mz);
        if (norm == 0.0f) return;     // handle NaN
        norm = 1.0f / norm;            // use reciprocal for division
        mx *= norm;
        my *= norm;
        mz *= norm;

        // Reference direction of Earth's magnetic field
        hx = 2.0f * mx * (0.5f - q3q3 - q4q4) + 2.0f * my * (q2q3 - q1q4) + 2.0f * mz * (q2q4 + q1q3);
        hy = 2.0f * mx * (q2q3 + q1q4) + 2.0f * my * (0.5f - q2q2 - q4q4) + 2.0f * mz * (q3q4 - q1q2);
        bx = sqrt((hx * hx) + (hy * hy));
        bz = 2.0f * mx * (q2q4 - q1q3) + 2.0f * my * (q3q4 + q1q2) + 2.0f * mz * (0.5f - q2q2 - q3q3);

        // Estimated direction of gravity and magnetic field
        vx = 2.0f * (q2q4 - q1q3);
        vy = 2.0f * (q1q2 + q3q4);
        vz = q1q1 - q2q2 - q3q3 + q4q4;
        wx = 2.0f * bx * (0.5f - q3q3 - q4q4) + 2.0f * bz * (q2q4 - q1q3);
        wy = 2.0f * bx * (q2q3 - q1q4) + 2.0f * bz * (q1q2 + q3q4);
        wz = 2.0f * bx * (q1q3 + q2q4) + 2.0f * bz * (0.5f - q2q2 - q3q3);

        // Error is cross product between estimated direction and measured direction of gravity
        ex = (ay * vz - az * vy) + (my * wz - mz * wy);
        ey = (az * vx - ax * vz) + (mz * wx - mx * wz);
        ez = (ax * vy - ay * vx) + (mx * wy - my * wx);
        if (Ki > 0.0f)
        {
                eInt[0] += ex;      // accumulate integral error
                eInt[1] += ey;
                eInt[2] += ez;
        }
        else
        {
                eInt[0] = 0.0f;     // prevent integral wind up
                eInt[1] = 0.0f;
                eInt[2] = 0.0f;
        }

        // Apply feedback terms
        gx = gx + Kp * ex + Ki * eInt[0];
        gy = gy + Kp * ey + Ki * eInt[1];
        gz = gz + Kp * ez + Ki * eInt[2];

        // Integrate rate of change of quaternion
        pa = q2;
        pb = q3;
        pc = q4;
        q1 = q1 + (-q2 * gx - q3 * gy - q4 * gz) * (0.5f * deltat);
        q2 = pa + (q1 * gx + pb * gz - pc * gy) * (0.5f * deltat);
        q3 = pb + (q1 * gy - pa * gz + pc * gx) * (0.5f * deltat);
        q4 = pc + (q1 * gz + pa * gy - pb * gx) * (0.5f * deltat);

        // Normalise quaternion
        norm = sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);
        norm = 1.0f / norm;
        q[0] = q1 * norm;
        q[1] = q2 * norm;
        q[2] = q3 * norm;
        q[3] = q4 * norm;

}





void mpu9250_test(void) {

        DEBUG_FUNCTION_ENTER


        // float magCalibration[3] = {0, 0, 0}, magbias[3] = {0, 0, 0};  // Factory mag calibration and mag bias
        // float gyroBias[3] = {0, 0, 0}, accelBias[3] = {0, 0, 0}; // Bias corrections for gyro and accelerometer
        // float ax, ay, az, gx, gy, gz, mx, my, mz; // variables to hold latest sensor data values

        static temp_value_t t_raw;

        static accel_values_t a_raw;

        static gyro_values_t g_raw;

        static mag_values_t m_raw;



        static accel_t a;

        static temp_t t;

        static gyro_t g;

        static mag_t m;

        int delt_t = 0; // used to control display output rate
        int count = 0;  // used to control display output rate


        float sum = 0;
        uint32_t sumCount = 0;
        char buffer[14];

        float SelfTest[6];

        int16_t tempCount;   // Stores the real internal chip temperature in degrees Celsius
        float temperature;

        uint32_t time_start;
        uint32_t time_end;
        uint32_t time_generated_ticks;

        uint32_t err_code;

        err_code = mpu9250_setup();
        APP_ERROR_CHECK(err_code);



        DEBUG_INFINITE_LOOP

        while(1) {



                uint32_t err_code;
                // If intPin goes high, all data registers have new data
                if(mpu9250_data_ready) { // On interrupt, check if data ready interrupt



                        err_code = mpu9250_get_data((int16_t*)(&a_raw), (int16_t*)(&g_raw), (int16_t*)(&t_raw));
                        APP_ERROR_CHECK(err_code);

                        // Now we'll calculate the accleration value into actual g's
                        // ax = (float)accelCount[0]*aRes - accelBias[0]; // get actual g value, this depends on scale being set
                        // ay = (float)accelCount[1]*aRes - accelBias[1];
                        // az = (float)accelCount[2]*aRes - accelBias[2];

                        mpu9250_convert_raw_accel(&a, &a_raw);


                        // Calculate the gyro value into actual degrees per second
                        // gx = (float)gyroCount[0]*gRes - gyroBias[0]; // get actual gyro value, this depends on scale being set
                        // gy = (float)gyroCount[1]*gRes - gyroBias[1];
                        // gz = (float)gyroCount[2]*gRes - gyroBias[2];

                        mpu9250_convert_raw_gyro(&g, &g_raw);

                        err_code = mpu9250_read_mag(&m_raw); // Read the x/y/z adc values
                        APP_ERROR_CHECK(err_code);
                        // Calculate the magnetometer values in milliGauss
                        // Include factory calibration per data sheet and user environmental corrections
                        // mx = (float)magCount[0]*mRes*magCalibration[0] - magbias[0]; // get actual magnetometer value, this depends on scale being set
                        // my = (float)magCount[1]*mRes*magCalibration[1] - magbias[1];
                        // mz = (float)magCount[2]*mRes*magCalibration[2] - magbias[2];

                        mpu9250_convert_raw_magn(&m, &m_raw);

                        mpu9250_data_ready = false;
                }

                time_end = app_timer_cnt_get();
                err_code = app_timer_cnt_diff_compute(time_end, time_start, &time_generated_ticks);
                APP_ERROR_CHECK(err_code);

                Now = ticks_to_ms(time_generated_ticks);
                deltat = (float)((Now - lastUpdate)/1000000.0f); // set integration time by time elapsed since last filter update
                lastUpdate = Now;

                sum += deltat;
                sumCount++;

                //    if(lastUpdate - firstUpdate > 10000000.0f) {
                //     beta = 0.04;  // decrease filter gain after stabilized
                //     zeta = 0.015; // increasey bias drift gain after stabilized
                //   }

                // Pass gyro rate as rad/s
                //  mpu9250.MadgwickQuaternionUpdate(ax, ay, az, gx*PI/180.0f, gy*PI/180.0f, gz*PI/180.0f,  my,  mx, mz);
                MahonyQuaternionUpdate(a.x, a.y, a.z, g.x*PI/180.0f, g.y*PI/180.0f, g.z*PI/180.0f, m.y, m.x, m.z);

                // Serial print and/or display at 0.5 s rate independent of data rates
                time_end = app_timer_cnt_get();
                err_code = app_timer_cnt_diff_compute(time_end, time_start, &time_generated_ticks);
                APP_ERROR_CHECK(err_code);

                delt_t = ticks_to_ms(time_generated_ticks) - count;
                if (delt_t > 500) { // update LCD once per half-second independent of read rate

                        printf("ax = %f", 1000*a.x);
                        printf(" ay = %f", 1000*a.y);
                        printf(" az = %f  mg\n\r", 1000*a.z);

                        printf("gx = %f", g.x);
                        printf(" gy = %f", g.y);
                        printf(" gz = %f  deg/s\n\r", g.z);

                        printf("mx = %f", m.x);
                        printf(" my = %f", m.y);
                        printf(" mz = %f  mG\n\r", m.z);

                        tempCount = mpu9250_read_temp(); // Read the adc values
                        temperature = ((float) tempCount) / 333.87f + 21.0f; // Temperature in degrees Centigrade
                        printf("temperature = %f  C\n\r", temperature);

                        printf("q0 = %f\n\r", q[0]);
                        printf("q1 = %f\n\r", q[1]);
                        printf("q2 = %f\n\r", q[2]);
                        printf("q3 = %f\n\r", q[3]);

                        /*    lcd.clear();
                            printf("MPU9250", 0, 0);
                            printf("x   y   z", 0, 1);
                            sprintf(buffer, "%d %d %d mg", (int)(1000.0f*ax), (int)(1000.0f*ay), (int)(1000.0f*az));
                            printf(buffer, 0, 2);
                            sprintf(buffer, "%d %d %d deg/s", (int)gx, (int)gy, (int)gz);
                            printf(buffer, 0, 3);
                            sprintf(buffer, "%d %d %d mG", (int)mx, (int)my, (int)mz);
                            printf(buffer, 0, 4);
                         */
                        // Define output variables from updated quaternion---these are Tait-Bryan angles, commonly used in aircraft orientation.
                        // In this coordinate system, the positive z-axis is down toward Earth.
                        // Yaw is the angle between Sensor x-axis and Earth magnetic North (or true North if corrected for local declination, looking down on the sensor positive yaw is counterclockwise.
                        // Pitch is angle between sensor x-axis and Earth ground plane, toward the Earth is positive, up toward the sky is negative.
                        // Roll is angle between sensor y-axis and Earth ground plane, y-axis up is positive roll.
                        // These arise from the definition of the homogeneous rotation matrix constructed from quaternions.
                        // Tait-Bryan angles as well as Euler angles are non-commutative; that is, the get the correct orientation the rotations must be
                        // applied in the correct order which for this configuration is yaw, pitch, and then roll.
                        // For more see http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles which has additional links.
                        yaw   = atan2(2.0f * (q[1] * q[2] + q[0] * q[3]), q[0] * q[0] + q[1] * q[1] - q[2] * q[2] - q[3] * q[3]);
                        pitch = -asin(2.0f * (q[1] * q[3] - q[0] * q[2]));
                        roll  = atan2(2.0f * (q[0] * q[1] + q[2] * q[3]), q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3]);
                        pitch *= 180.0f / PI;
                        yaw   *= 180.0f / PI;
                        yaw   -= 13.8f;// Declination at Danville, California is 13 degrees 48 minutes and 47 seconds on 2014-04-04
                        roll  *= 180.0f / PI;

                        printf("Yaw, Pitch, Roll: %f %f %f\n\r", yaw, pitch, roll);
                        printf("average rate = %f\n\r", (float) sumCount/sum);
                        //    sprintf(buffer, "YPR: %f %f %f", yaw, pitch, roll);
                        //    printf(buffer, 0, 4);
                        //    sprintf(buffer, "rate = %f", (float) sumCount/sum);
                        //    printf(buffer, 0, 5);

                        // myled= !myled;

                        // count = t.read_ms();
                        time_end = app_timer_cnt_get();
                        err_code = app_timer_cnt_diff_compute(time_end, time_start, &time_generated_ticks);
                        APP_ERROR_CHECK(err_code);

                        count = ticks_to_ms(time_generated_ticks);


                        if(count > 1<<21) {
                                //t.start(); // start the timer over again if ~30 minutes has passed
                                time_start = app_timer_cnt_get();
                                count = 0;
                                deltat= 0;


                                // lastUpdate = t.read_us();

                                time_end = app_timer_cnt_get();
                                err_code = app_timer_cnt_diff_compute(time_end, time_start, &time_generated_ticks);
                                APP_ERROR_CHECK(err_code);

                                lastUpdate = ticks_to_us(time_generated_ticks);


                        }
                        sum = 0;
                        sumCount = 0;
                }
        }


        DEBUG_FUNCTION_EXIT
}


void mpu9250_interrupt_test(void)
{

        DEBUG_FUNCTION_ENTER

        uint32_t sample_number = 0;

        float temperature;

        uint32_t err_code;

        static temp_value_t t_raw;

        static accel_values_t a_raw;

        static gyro_values_t g_raw;

        static mag_values_t m_raw;



        static temp_t t;

        static accel_t a;

        static gyro_t g;

        static mag_t m;



        err_code = mpu9250_setup();
        APP_ERROR_CHECK(err_code);


        DEBUG_INFINITE_LOOP

        printf("\033[H\033[J");

        while(1)
        {
                if(mpu9250_data_ready)
                {
                        // Make sure any pending events are cleared
                        __SEV();
                        __WFE();
                        // Enter System ON sleep mode
                        __WFE();
                        NRF_LOG_FLUSH();
                }

                // Read interrupt register to clear
                uint8_t int_status = mpu9250_read_single_byte(MPU9250_ADDRESS, INT_STATUS);

                NRF_LOG_DEBUG("INT STATUS = %d\n\r", int_status);

                err_code = mpu9250_get_data((int16_t*)(&a_raw), (int16_t*)(&g_raw), (int16_t*)(&t_raw));
                APP_ERROR_CHECK(err_code);

                err_code = mpu9250_read_mag((int16_t*)(&m_raw));
                APP_ERROR_CHECK(err_code);



                mpu9250_convert_raw_accel(&a, &a_raw);
                mpu9250_convert_raw_gyro(&g, &g_raw);
                mpu9250_convert_raw_magn(&m, &m_raw);

                temperature = ((float) t) / 333.87f + 21.0f; // Temperature in degrees Centigrade

                // printf("Sample  # %d\r\n", ++sample_number);


                printf("RAW ACCEL  | X: %10d   Y: %10d    Z: %10d\r\n", a_raw.x, a_raw.y, a_raw.z);
                printf("RAW GYRO   | X: %10d   Y: %10d    Z: %10d\r\n", g_raw.x, g_raw.y, g_raw.z);
                printf("RAW MAGN   | X: %10d   Y: %10d    Z: %10d\r\n", m_raw.x, m_raw.y, m_raw.z);

                printf("\r\n");

                // Clear terminal and print values

                printf("CONV ACCEL | X: %10f   Y: %10f    Z: %10f\r\n", a.x, a.y, a.z);
                printf("CONV GYRO  | X: %10f   Y: %10f    Z: %10f\r\n", g.x, g.y, g.z);
                printf("CONV MAGN  | X: %10f   Y: %10f    Z: %10f\r\n", m.x, m.y, m.z);




                printf("TEMP       | %f\r\n\r\n", temperature);
                // printf("\x1B[7A"); // VT100 escape code: move cursor up 14 lines

                mpu9250_data_ready = false;

        }

        // mpu9250_process_raw_data(&accel_isr, &gyro_isr, &temp_isr);
//
// printf("ax = %f",  accel_isr->x);
// printf(" ay = %f", accel_isr->y);
// printf(" az = %f  mg\n\r", accel_isr->z);
//
// printf("gx = %f",  gyro_isr->x);
// printf(" gy = %f", gyro_isr->y);
// printf(" gz = %f  deg/s\n\r", gyro_isr->z);
//
//
//
// temperature = ((float) temp_isr) / 333.87f + 21.0f; // Temperature in degrees Centigrade
// printf("temperature = %f  C\n\r", temperature);
//
        DEBUG_FUNCTION_EXIT

}
