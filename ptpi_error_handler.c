/** @file
 *
 * @defgroup app_error Common application error handler
 * @{
 * @ingroup app_common
 *
 * @brief Common application error handler.
 */

#include "ptpi_bsp.h"


#include "nrf.h"
#include <stdio.h>
#include "app_error.h"
#include "nordic_common.h"
#include "sdk_errors.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"






/**
 * Function is implemented as weak so that it can be overwritten by custom application error handler
 * when needed.
 */
void app_error_fault_handler(uint32_t id, uint32_t pc, uint32_t info)
{
        // On assert, the system can only recover with a reset.
#ifndef DEBUG
//    NVIC_SystemReset(); // Override this if you like, so it does not reboot
#else

#ifdef BSP_DEFINES_ONLY
//    LEDS_ON(PTPI_POWER_LED); // Only runs if you have Board Support Package with LEDs configured
#else
        UNUSED_VARIABLE(bsp_indication_set(BSP_INDICATE_FATAL_ERROR));
        // This call can be used for debug purposes during application development.
        // @note CAUTION: Activating this code will write the stack to flash on an error.
        //                This function should NOT be used in a final product.
        //                It is intended STRICTLY for development/debugging purposes.
        //                The flash write will happen EVEN if the radio is active, thus interrupting
        //                any communication.
        //                Use with care. Uncomment the line below to use.
        //ble_debug_assert_handler(error_code, line_num, p_file_name);
#endif // BSP_DEFINES_ONLY

        app_error_log(id, pc, info);
        app_error_print(id, pc, info);
        // app_error_save_and_stop(id, pc, info); // You may want to continue your code execution.

#endif // DEBUG
}


// Implementation of Hard Fault Handler. It dispays address of fault.
void HardFault_Handler(void)
{
        uint32_t *sp = (uint32_t *) __get_MSP(); // Get stack pointer
        uint32_t ia = sp[24/4]; // Get instruction address from stack

        printf("Hard Fault at address: 0x%08x\r\n", (unsigned int)ia);
        while(1)
                ;
}
